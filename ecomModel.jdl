/**
* !!!!!!  IMPORTANT where we are removing fields in java domain classes  corresponding modification should be done on the vuejs model classess
*/
/** AppUser
 * 1. After  code generation the java class (in package com.ic.ecomm.domain) AppUser.java @Table annotation should be changed to authentication
 * 2. Remove the generated Id filed from AppUser.java and annotate customerId filed with @Id as in the database table customer_id id the PK
 * 3. Change the field annotation for firstName and lastName to match with FB table (firstname, lastname)
 * 4. Change the filedanntation of isBlocked to is_block to match with DB table. similarly lastlogin,otpTimestamp,timestamp
 */
 /**
 *com.ic.ecomm.web.rest.AppUserResource class methods createAppUser & updateAppUser needs to modified to reflect customerId as the Identifier filed.
 *
 */
entity AppUser {
    customerId String,
    clientId String,
    identifierId String,
    createdAt Instant,
    email String,
    firstName String ,
    isBlocked Boolean,
    isCustomer Boolean,
    isInactive Boolean,
    isVendor Boolean,
    lastLogin Instant,
    lastName String ,
    loginOtp String ,
    loginStatus Integer,
    loginCount Integer,
    otpTimestamp Instant,
    password String,
    phone String,
    resetToken String,
    timestamp Instant,
    updatedAt Instant,
    userRole String,
    userName String,
    isAgent Boolean,
    isBankCustomer Boolean,
    verifiedAccount Boolean,
	lastLoginCountry String,
	lastLoginDeviceId String,
	lastLoginIpAddress String,
	failureLoginAttempts Integer,
	
}
enum DeviceStatus {
    ACTIVE,INACTIVE,SUSPENDED
}
enum DeviceType {
    MOBILE,DESKTOP,TABLET,VOICE
}

entity AppUserDevice {
	deviceId String  ,
	deviceType DeviceType,
	deviceOs String,
	country String,
	ipAddressDuringRegistration String,
	createdAt Instant,
	deviceKey String,
	status DeviceStatus,
	pin String,
	isLoggedIn Boolean,
	lastLoginCountry String,
	lastLoginIpAddress String
}

relationship OneToMany {
	AppUser{device} to AppUserDevice{appUser(userName)}
	
}


/** ProductCategory
 * 1. Change the field annotation for depthLevel to match with FB table ()
 */
entity ProductCategory {
	clientId String ,
	adminKey String ,
	backgroundImage String ,
	categoryId String ,
	depthLevel Integer,
	description String ,
	name String ,
	parentCategoryId String ,
	recordStatus Integer
}

relationship ManyToOne {
	Product{productCategory(name)} to ProductCategory,
	Product{brand(name)} to Brand
	
}


/** Brand
 * 1. After  code generation the java class (in package com.ic.ecomm.domain) Brand.java @Table annotation should be changed to brands
 * 2. Remove the generated Id filed from Brand.java and annotate brandId filed with @Id as in the database table brand_id id the PK
 * 3. Change the column annotation of name filed to brand_name to match with DB table.
 */
  /**
 *com.ic.ecomm.web.rest.Brand class methods createBrand & updateBrand needs to modified to reflect bandId as the Identifier filed.
 *
 */
entity Brand {
	brandId String,
	name String	
}


/** Product
 * 1. Change the annotation of brand & productCategoryId to include @JoinColumn &  @org.hibernate.annotations.ForeignKey(name = "none"). Since there is no foriegn key connection between product table
 * with Brand table or ProductCategory table.
 */
entity Product{
	productId String ,
	clientId String ,
	adminKey String ,
	createdAt Instant,
	description String  ,
	modelCode String  ,
	name String ,
	recordStatus Integer ,
	salesQuantity BigDecimal,
	tagCode String ,
	updatedAt Instant,
	indentifierId String ,
	identifierId String ,
	productGroupId String
}

/** ProductDefinition
 * 1. Change the annotation of product  to include @JoinColumn &  @org.hibernate.annotations.ForeignKey(name = "none"). Since there is no foriegn key connection between product table
 * with products table.
 */
entity ProductDefinition{
	clientId String ,
	adminKey String ,
	productCategoryId String ,
	properties String ,
	propertyName String ,
	propertyValue String ,
	recordStatus Integer ,
	forFilter Boolean
}

relationship ManyToOne {
	ProductDefinition{product(name)} to Product
	ProductGroup{product(name)} to Product
}

relationship OneToOne {
	ProductDetails{product(name)} to Product
	
}

/** ProductDetails - This will create a conflict on VueJs side there is Product component and ProductDetails component  in that section. The listing Vue component needs to be modified,
 * 1. Change the annotation of product  to include @JoinColumn &  @org.hibernate.annotations.ForeignKey(name = "none"). Since there is no foriegn key connection between product table
 * with products table.
 * The Coulmn anntations of image1, image2, image3, image4 needs to be changed to reflect db table
 */
entity ProductDetails{
	clientId String ,
	adminKey String  ,
	background String  ,
	description String  ,
	image1 String  ,
	image2 String  ,
	image3 String  ,
	image4 String  ,
	productCategoryId String   ,
	recordStatus Integer ,
	shortDescription String ,
	unit String  ,
	weight BigDecimal
}


/** ProductInventory - 
 * 1. Not linking ProdcutInventory  with product now. this might need redesign. Now this is a standalone item.
 */
entity ProductInventory{
	productId String   ,
	clientId String   ,
	availableStock Integer ,
	costPrice BigDecimal ,
	damagedQuantity Integer ,
	maxQuantity Integer ,
	priceTag String  ,
	productCategoryId String   ,
	recordStatus Integer ,
	reorderLevel Integer ,
	unitPrice BigDecimal
}

/** ProductInventory - 
 * 1. Change the annotation of product  to include @JoinColumn &  @org.hibernate.annotations.ForeignKey(name = "none"). Since there is no foriegn key connection between product table
 * with products table.
 */
entity ProductGroup{
	clientId String   ,
	adminKey String  ,
	groupKey String   ,
	groupValue String   ,
	productCategoryId String   ,
	productGroupId String 

}

enum FullOrderStatus {
    Delivered, Shipped, Processing
}
enum OrderStatus {
    Delivered, Shipped, Processing
}
enum PaymentStatus {
    SUCCESS, PENDING, FAILED
}



/** Order - 
 * 1. make sure that the Table annotation has correct table name - order_info.
 * 2. Remove the Id column and make orderNumber as key with @Id annotation - db table filed ame order_number
 */
entity Order{
	orderNumber String,
	clientId String  ,
	address String ,
	paymentGateway String  ,
	conversionValue BigDecimal,
	createdAt Instant,
	customerChoosenCurrency String  ,
	customerCurrencySymbol String  ,
	customerId String  ,
	defaultPaymentCurrency String  ,
	email String  ,
	fullOrderStatus FullOrderStatus ,
	isFinalize Boolean,
	items String ,
	orderDate Instant,
	paymentId String  ,
	paymentMethod String  ,
	paymentStatus PaymentStatus  ,
	totalPrice Integer,
	updatedAt Instant,
	earnedCoins Integer,
	fakeItems String  ,
	giftCardAmount Integer,
	giftCardId String  ,
	orderType String  ,
	paymentDate Instant,
	redeemCoinAmount Integer,
	redeemCoins Integer,
	totalAmountToPay Integer,
	appliedCouponAmount Integer,
	appliedPromoCode String  ,
	shippingCharge BigDecimal,
	tax BigDecimal,
}
entity OrderDetails{
	orderDetailId Integer ,
	clientId String  ,
	createdAt Instant,
	customerAddress String ,
	customerId String  ,
	deliveredDate Instant,
	email String  ,
	fullOrderStatus FullOrderStatus ,
	isProgressed Boolean,
	isShipped Boolean,
	orderDate Instant,
	orderNumber String  ,
	orderStatus OrderStatus  ,
	paymentStatus String  ,
	price BigDecimal,
	productId String  ,
	productImage String  ,
	productName String  ,
	quantity Integer,
	shippedDate Instant,
	paymentMethod String  ,
	subTotal BigDecimal,
	timeStamp Instant,
	trackingId String  ,
	trackingUrl String  ,
	updatedAt Instant,
	vendorId String  ,
	verificationKey String  ,
	earnedCoins Integer,
	giftCardAmount BigDecimal,
	giftCardId String  ,
	orderType String  ,
	paymentDate Instant,
	redeemCoinAmount BigDecimal,
	redeemCoins Integer,
	shippingCharge BigDecimal,
	tax BigDecimal,
	orderTotal BigDecimal,
	appliedCouponAmount BigDecimal,
	appliedPromoCode String
}

relationship ManyToOne {
	OrderDetails{order(orderNumber)} to Order

	
}

paginate Product with pager
paginate Order with pager