import Vue from 'vue';
import Component from 'vue-class-component';
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate' // for vue-router 2.2+
]);
import Router from 'vue-router';
const Home = () => import('../core/home/home.vue');
const Error = () => import('../core/error/error.vue');
const Register = () => import('../account/register/register.vue');
const Activate = () => import('../account/activate/activate.vue');
const ResetPasswordInit = () => import('../account/reset-password/init/reset-password-init.vue');
const ResetPasswordFinish = () => import('../account/reset-password/finish/reset-password-finish.vue');
const ChangePassword = () => import('../account/change-password/change-password.vue');
const Settings = () => import('../account/settings/settings.vue');
const JhiUserManagementComponent = () => import('../admin/user-management/user-management.vue');
const JhiUserManagementViewComponent = () => import('../admin/user-management/user-management-view.vue');
const JhiUserManagementEditComponent = () => import('../admin/user-management/user-management-edit.vue');
const Sessions = () => import('../account/sessions/sessions.vue');
const JhiConfigurationComponent = () => import('../admin/configuration/configuration.vue');
const JhiDocsComponent = () => import('../admin/docs/docs.vue');
const JhiHealthComponent = () => import('../admin/health/health.vue');
const JhiLogsComponent = () => import('../admin/logs/logs.vue');
const JhiAuditsComponent = () => import('../admin/audits/audits.vue');
const JhiMetricsComponent = () => import('../admin/metrics/metrics.vue');
/* tslint:disable */
// prettier-ignore
const AppUser = () => import('../entities/app-user/app-user.vue');
// prettier-ignore
const AppUserUpdate = () => import('../entities/app-user/app-user-update.vue');
// prettier-ignore
const AppUserDetails = () => import('../entities/app-user/app-user-details.vue');
// prettier-ignore
const AppUserDevice = () => import('../entities/app-user-device/app-user-device.vue');
// prettier-ignore
const AppUserDeviceUpdate = () => import('../entities/app-user-device/app-user-device-update.vue');
// prettier-ignore
const AppUserDeviceDetails = () => import('../entities/app-user-device/app-user-device-details.vue');
// prettier-ignore
const ProductCategory = () => import('../entities/product-category/product-category.vue');
// prettier-ignore
const ProductCategoryUpdate = () => import('../entities/product-category/product-category-update.vue');
// prettier-ignore
const ProductCategoryDetails = () => import('../entities/product-category/product-category-details.vue');
// prettier-ignore
const Brand = () => import('../entities/brand/brand.vue');
// prettier-ignore
const BrandUpdate = () => import('../entities/brand/brand-update.vue');
// prettier-ignore
const BrandDetails = () => import('../entities/brand/brand-details.vue');
// prettier-ignore
const Product = () => import('../entities/product/product.vue');
// prettier-ignore
const ProductUpdate = () => import('../entities/product/product-update.vue');
// prettier-ignore
const ProductDetails = () => import('../entities/product/product-details.vue');
// prettier-ignore
const ProductDefinition = () => import('../entities/product-definition/product-definition.vue');
// prettier-ignore
const ProductDefinitionUpdate = () => import('../entities/product-definition/product-definition-update.vue');
// prettier-ignore
const ProductDefinitionDetails = () => import('../entities/product-definition/product-definition-details.vue');
// prettier-ignore
const ProductDetailsList = () => import('../entities/product-details/product-details-list.vue');
// prettier-ignore
const ProductDetailsUpdate = () => import('../entities/product-details/product-details-update.vue');
// prettier-ignore
const ProductDetailsDetails = () => import('../entities/product-details/product-details-details.vue');
// prettier-ignore
const ProductInventory = () => import('../entities/product-inventory/product-inventory.vue');
// prettier-ignore
const ProductInventoryUpdate = () => import('../entities/product-inventory/product-inventory-update.vue');
// prettier-ignore
const ProductInventoryDetails = () => import('../entities/product-inventory/product-inventory-details.vue');
// prettier-ignore
const ProductGroup = () => import('../entities/product-group/product-group.vue');
// prettier-ignore
const ProductGroupUpdate = () => import('../entities/product-group/product-group-update.vue');
// prettier-ignore
const ProductGroupDetails = () => import('../entities/product-group/product-group-details.vue');
// prettier-ignore
const Order = () => import('../entities/order/order.vue');
// prettier-ignore
const OrderUpdate = () => import('../entities/order/order-update.vue');
// prettier-ignore
//const OrderDetails = () => import('../entities/order/order-details.vue');
// prettier-ignore
const OrderDetails = () => import('../entities/order-details/order-details.vue');
// prettier-ignore
const OrderDetailsUpdate = () => import('../entities/order-details/order-details-update.vue');
// prettier-ignore
const OrderDetailsDetails = () => import('../entities/order-details/order-details-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

Vue.use(Router);

// prettier-ignore
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/forbidden',
      name: 'Forbidden',
      component: Error,
      meta: { error403: true }
    },
    {
      path: '/not-found',
      name: 'NotFound',
      component: Error,
      meta: { error404: true }
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/activate',
      name: 'Activate',
      component: Activate
    },
    {
      path: '/reset/request',
      name: 'ResetPasswordInit',
      component: ResetPasswordInit
    },
    {
      path: '/reset/finish',
      name: 'ResetPasswordFinish',
      component: ResetPasswordFinish
    },
    {
      path: '/account/password',
      name: 'ChangePassword',
      component: ChangePassword,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/account/sessions',
      name: 'Sessions',
      component: Sessions,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/account/settings',
      name: 'Settings',
      component: Settings,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/admin/user-management',
      name: 'JhiUser',
      component: JhiUserManagementComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/user-management/new',
      name: 'JhiUserCreate',
      component: JhiUserManagementEditComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/user-management/:userId/edit',
      name: 'JhiUserEdit',
      component: JhiUserManagementEditComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/user-management/:userId/view',
      name: 'JhiUserView',
      component: JhiUserManagementViewComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/docs',
      name: 'JhiDocsComponent',
      component: JhiDocsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/audits',
      name: 'JhiAuditsComponent',
      component: JhiAuditsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/jhi-health',
      name: 'JhiHealthComponent',
      component: JhiHealthComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/logs',
      name: 'JhiLogsComponent',
      component: JhiLogsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/jhi-metrics',
      name: 'JhiMetricsComponent',
      component: JhiMetricsComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    },
    {
      path: '/admin/jhi-configuration',
      name: 'JhiConfigurationComponent',
      component: JhiConfigurationComponent,
      meta: { authorities: ['ROLE_ADMIN'] }
    }
    ,
    {
      path: '/entity/app-user',
      name: 'AppUser',
      component: AppUser,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/app-user/new',
      name: 'AppUserCreate',
      component: AppUserUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/app-user/:appUserId/edit',
      name: 'AppUserEdit',
      component: AppUserUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/app-user/:appUserId/view',
      name: 'AppUserView',
      component: AppUserDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/app-user-device',
      name: 'AppUserDevice',
      component: AppUserDevice,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/app-user-device/new',
      name: 'AppUserDeviceCreate',
      component: AppUserDeviceUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/app-user-device/:appUserDeviceId/edit',
      name: 'AppUserDeviceEdit',
      component: AppUserDeviceUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/app-user-device/:appUserDeviceId/view',
      name: 'AppUserDeviceView',
      component: AppUserDeviceDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/product-category',
      name: 'ProductCategory',
      component: ProductCategory,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-category/new',
      name: 'ProductCategoryCreate',
      component: ProductCategoryUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-category/:productCategoryId/edit',
      name: 'ProductCategoryEdit',
      component: ProductCategoryUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-category/:productCategoryId/view',
      name: 'ProductCategoryView',
      component: ProductCategoryDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/brand',
      name: 'Brand',
      component: Brand,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/brand/new',
      name: 'BrandCreate',
      component: BrandUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/brand/:brandId/edit',
      name: 'BrandEdit',
      component: BrandUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/brand/:brandId/view',
      name: 'BrandView',
      component: BrandDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/product',
      name: 'Product',
      component: Product,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product/new',
      name: 'ProductCreate',
      component: ProductUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product/:productId/edit',
      name: 'ProductEdit',
      component: ProductUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product/:productId/view',
      name: 'ProductView',
      component: ProductDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/product-definition',
      name: 'ProductDefinition',
      component: ProductDefinition,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-definition/new',
      name: 'ProductDefinitionCreate',
      component: ProductDefinitionUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-definition/:productDefinitionId/edit',
      name: 'ProductDefinitionEdit',
      component: ProductDefinitionUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-definition/:productDefinitionId/view',
      name: 'ProductDefinitionView',
      component: ProductDefinitionDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/product-details-list',
      name: 'ProductDetailsList', 
      component: ProductDetailsList,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-details/new',
      name: 'ProductDetailsCreate',
      component: ProductDetailsUpdate,
      meta: { authorities: ['ROLE_USER'] }   
    },
    {
      path: '/entity/product-details/:productDetailsId/edit',
      name: 'ProductDetailsEdit',
      component: ProductDetailsUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-details/:productDetailsId/view',
      name: 'ProductDetailsView',
      component: ProductDetailsDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/product-inventory',
      name: 'ProductInventory',
      component: ProductInventory,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-inventory/new',
      name: 'ProductInventoryCreate',
      component: ProductInventoryUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-inventory/:productInventoryId/edit',
      name: 'ProductInventoryEdit',
      component: ProductInventoryUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-inventory/:productInventoryId/view',
      name: 'ProductInventoryView',
      component: ProductInventoryDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/product-group',
      name: 'ProductGroup',
      component: ProductGroup,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-group/new',
      name: 'ProductGroupCreate',
      component: ProductGroupUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-group/:productGroupId/edit',
      name: 'ProductGroupEdit',
      component: ProductGroupUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/product-group/:productGroupId/view',
      name: 'ProductGroupView',
      component: ProductGroupDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/order',
      name: 'Order',
      component: Order,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/order/new',
      name: 'OrderCreate',
      component: OrderUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/order/:orderId/edit',
      name: 'OrderEdit',
      component: OrderUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/order/:orderId/view',
      name: 'OrderView',
      component: OrderDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    ,
    {
      path: '/entity/order-details',
      name: 'OrderDetails',
      component: OrderDetails,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/order-details/new',
      name: 'OrderDetailsCreate',
      component: OrderDetailsUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/order-details/:orderDetailsId/edit',
      name: 'OrderDetailsEdit',
      component: OrderDetailsUpdate,
      meta: { authorities: ['ROLE_USER'] }
    },
    {
      path: '/entity/order-details/:orderDetailsId/view',
      name: 'OrderDetailsView',
      component: OrderDetailsDetails,
      meta: { authorities: ['ROLE_USER'] }
    }
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ]
});
