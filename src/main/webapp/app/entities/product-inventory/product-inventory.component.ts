import { Component, Inject, Vue } from 'vue-property-decorator';
import { IProductInventory } from '@/shared/model/product-inventory.model';
import AlertService from '@/shared/alert/alert.service';

import ProductInventoryService from './product-inventory.service';

@Component
export default class ProductInventory extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productInventoryService') private productInventoryService: () => ProductInventoryService;
  private removeId: number = null;
  public productInventories: IProductInventory[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllProductInventorys();
  }

  public clear(): void {
    this.retrieveAllProductInventorys();
  }

  public retrieveAllProductInventorys(): void {
    this.isFetching = true;

    this.productInventoryService()
      .retrieve()
      .then(
        res => {
          this.productInventories = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IProductInventory): void {
    this.removeId = instance.id;
  }

  public removeProductInventory(): void {
    this.productInventoryService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerchantApp.productInventory.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllProductInventorys();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
