import { Component, Vue, Inject } from 'vue-property-decorator';

import { IProductInventory } from '@/shared/model/product-inventory.model';
import ProductInventoryService from './product-inventory.service';

@Component
export default class ProductInventoryDetails extends Vue {
  @Inject('productInventoryService') private productInventoryService: () => ProductInventoryService;
  public productInventory: IProductInventory = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productInventoryId) {
        vm.retrieveProductInventory(to.params.productInventoryId);
      }
    });
  }

  public retrieveProductInventory(productInventoryId) {
    this.productInventoryService()
      .find(productInventoryId)
      .then(res => {
        this.productInventory = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
