import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import ProductService from '../product/product.service';
import { IProduct } from '@/shared/model/product.model';

import AlertService from '@/shared/alert/alert.service';
import { IProductInventory, ProductInventory } from '@/shared/model/product-inventory.model';
import ProductInventoryService from './product-inventory.service';

const validations: any = {
  productInventory: {
    clientId: {},
    availableStock: {},
    costPrice: {},
    damagedQuantity: {},
    maxQuantity: {},
    priceTag: {},
    productCategoryId: {},
    recordStatus: {},
    reorderLevel: {},
    unitPrice: {}
  }
};

@Component({
  validations
})
export default class ProductInventoryUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productInventoryService') private productInventoryService: () => ProductInventoryService;
  public productInventory: IProductInventory = new ProductInventory();

  @Inject('productService') private productService: () => ProductService;

  public products: IProduct[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productInventoryId) {
        vm.retrieveProductInventory(to.params.productInventoryId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.productInventory.id) {
      this.productInventoryService()
        .update(this.productInventory)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productInventory.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.productInventoryService()
        .create(this.productInventory)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productInventory.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveProductInventory(productInventoryId): void {
    this.productInventoryService()
      .find(productInventoryId)
      .then(res => {
        this.productInventory = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.productService()
      .retrieve()
      .then(res => {
        this.products = res.data;
      });
  }
}
