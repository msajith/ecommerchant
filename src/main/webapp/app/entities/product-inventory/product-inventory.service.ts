import axios from 'axios';

import { IProductInventory } from '@/shared/model/product-inventory.model';

const baseApiUrl = 'api/product-inventories';

export default class ProductInventoryService {
  public find(id: number): Promise<IProductInventory> {
    return new Promise<IProductInventory>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IProductInventory): Promise<IProductInventory> {
    return new Promise<IProductInventory>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IProductInventory): Promise<IProductInventory> {
    return new Promise<IProductInventory>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
