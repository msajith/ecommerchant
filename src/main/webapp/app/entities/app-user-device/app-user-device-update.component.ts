import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import { DATE_TIME_LONG_FORMAT, INSTANT_FORMAT, ZONED_DATE_TIME_FORMAT } from '@/shared/date/filters';

import AppUserService from '../app-user/app-user.service';
import { IAppUser } from '@/shared/model/app-user.model';

import AlertService from '@/shared/alert/alert.service';
import { IAppUserDevice, AppUserDevice } from '@/shared/model/app-user-device.model';
import AppUserDeviceService from './app-user-device.service';

const validations: any = {
  appUserDevice: {
    deviceId: {},
    deviceType: {},
    deviceOs: {},
    country: {},
    ipAddressDuringRegistration: {},
    createdAt: {},
    deviceKey: {},
    status: {},
    pin: {},
    isLoggedIn: {},
    lastLoginCountry: {},
    lastLoginIpAddress: {}
  }
};

@Component({
  validations
})
export default class AppUserDeviceUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('appUserDeviceService') private appUserDeviceService: () => AppUserDeviceService;
  public appUserDevice: IAppUserDevice = new AppUserDevice();

  @Inject('appUserService') private appUserService: () => AppUserService;

  public appUsers: IAppUser[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.appUserDeviceId) {
        vm.retrieveAppUserDevice(to.params.appUserDeviceId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.appUserDevice.id) {
      this.appUserDeviceService()
        .update(this.appUserDevice)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.appUserDevice.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.appUserDeviceService()
        .create(this.appUserDevice)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.appUserDevice.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.appUserDevice[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.appUserDevice[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.appUserDevice[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.appUserDevice[field] = null;
    }
  }

  public retrieveAppUserDevice(appUserDeviceId): void {
    this.appUserDeviceService()
      .find(appUserDeviceId)
      .then(res => {
        this.appUserDevice = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.appUserService()
      .retrieve()
      .then(res => {
        this.appUsers = res.data;
      });
  }
}
