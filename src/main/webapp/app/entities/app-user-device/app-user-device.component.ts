import { Component, Inject, Vue } from 'vue-property-decorator';
import { IAppUserDevice } from '@/shared/model/app-user-device.model';
import AlertService from '@/shared/alert/alert.service';

import AppUserDeviceService from './app-user-device.service';

@Component
export default class AppUserDevice extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('appUserDeviceService') private appUserDeviceService: () => AppUserDeviceService;
  private removeId: number = null;
  public appUserDevices: IAppUserDevice[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllAppUserDevices();
  }

  public clear(): void {
    this.retrieveAllAppUserDevices();
  }

  public retrieveAllAppUserDevices(): void {
    this.isFetching = true;

    this.appUserDeviceService()
      .retrieve()
      .then(
        res => {
          this.appUserDevices = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IAppUserDevice): void {
    this.removeId = instance.id;
  }

  public removeAppUserDevice(): void {
    this.appUserDeviceService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerchantApp.appUserDevice.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllAppUserDevices();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
