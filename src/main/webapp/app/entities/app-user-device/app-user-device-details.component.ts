import { Component, Vue, Inject } from 'vue-property-decorator';

import { IAppUserDevice } from '@/shared/model/app-user-device.model';
import AppUserDeviceService from './app-user-device.service';

@Component
export default class AppUserDeviceDetails extends Vue {
  @Inject('appUserDeviceService') private appUserDeviceService: () => AppUserDeviceService;
  public appUserDevice: IAppUserDevice = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.appUserDeviceId) {
        vm.retrieveAppUserDevice(to.params.appUserDeviceId);
      }
    });
  }

  public retrieveAppUserDevice(appUserDeviceId) {
    this.appUserDeviceService()
      .find(appUserDeviceId)
      .then(res => {
        this.appUserDevice = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
