import axios from 'axios';

import { IAppUserDevice } from '@/shared/model/app-user-device.model';

const baseApiUrl = 'api/app-user-devices';

export default class AppUserDeviceService {
  public find(id: number): Promise<IAppUserDevice> {
    return new Promise<IAppUserDevice>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IAppUserDevice): Promise<IAppUserDevice> {
    return new Promise<IAppUserDevice>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IAppUserDevice): Promise<IAppUserDevice> {
    return new Promise<IAppUserDevice>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
