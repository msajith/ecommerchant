import { Component, Vue, Inject } from 'vue-property-decorator';

import { IOrderDetails } from '@/shared/model/order-details.model';
import OrderDetailsService from './order-details.service';

@Component
export default class OrderDetailsDetails extends Vue {
  @Inject('orderDetailsService') private orderDetailsService: () => OrderDetailsService;
  public orderDetails: IOrderDetails = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderDetailsId) {
        vm.retrieveOrderDetails(to.params.orderDetailsId);
      }
    });
  }

  public retrieveOrderDetails(orderDetailsId) {
    this.orderDetailsService()
      .find(orderDetailsId)
      .then(res => {
        this.orderDetails = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
