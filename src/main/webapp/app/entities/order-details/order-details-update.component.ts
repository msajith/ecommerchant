import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import { DATE_TIME_LONG_FORMAT, INSTANT_FORMAT, ZONED_DATE_TIME_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';
import { IOrderDetails, OrderDetails } from '@/shared/model/order-details.model';
import OrderDetailsService from './order-details.service';

const validations: any = {
  orderDetails: {
    orderDetailId: {},
    clientId: {},
    createdAt: {},
    customerAddress: {},
    customerId: {},
    deliveredDate: {},
    email: {},
    fullOrderStatus: {},
    isProgressed: {},
    isShipped: {},
    orderDate: {},
    orderNumber: {},
    orderStatus: {},
    paymentStatus: {},
    price: {},
    productId: {},
    productImage: {},
    productName: {},
    quantity: {},
    shippedDate: {},
    paymentMethod: {},
    subTotal: {},
    timeStamp: {},
    trackingId: {},
    trackingUrl: {},
    updatedAt: {},
    vendorId: {},
    verificationKey: {},
    earnedCoins: {},
    giftCardAmount: {},
    giftCardId: {},
    orderType: {},
    paymentDate: {},
    redeemCoinAmount: {},
    redeemCoins: {},
    shippingCharge: {},
    tax: {},
    orderTotal: {},
    appliedCouponAmount: {},
    appliedPromoCode: {}
  }
};

@Component({
  validations
})
export default class OrderDetailsUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('orderDetailsService') private orderDetailsService: () => OrderDetailsService;
  public orderDetails: IOrderDetails = new OrderDetails();
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderDetailsId) {
        vm.retrieveOrderDetails(to.params.orderDetailsId);
      }
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.orderDetails.id) {
      this.orderDetailsService()
        .update(this.orderDetails)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.orderDetails.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.orderDetailsService()
        .create(this.orderDetails)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.orderDetails.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.orderDetails[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.orderDetails[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.orderDetails[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.orderDetails[field] = null;
    }
  }

  public retrieveOrderDetails(orderDetailsId): void {
    this.orderDetailsService()
      .find(orderDetailsId)
      .then(res => {
        this.orderDetails = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
