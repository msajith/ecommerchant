import { Component, Inject, Vue } from 'vue-property-decorator';
import { IOrderDetails } from '@/shared/model/order-details.model';
import AlertService from '@/shared/alert/alert.service';

import OrderDetailsService from './order-details.service';

@Component
export default class OrderDetails extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('orderDetailsService') private orderDetailsService: () => OrderDetailsService;
  private removeId: number = null;
  public orderDetails: IOrderDetails[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllOrderDetailss();
  }

  public clear(): void {
    this.retrieveAllOrderDetailss();
  }

  public retrieveAllOrderDetailss(): void {
    this.isFetching = true;

    this.orderDetailsService()
      .retrieve()
      .then(
        res => {
          this.orderDetails = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IOrderDetails): void {
    this.removeId = instance.id;
  }

  public removeOrderDetails(): void {
    this.orderDetailsService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerchantApp.orderDetails.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllOrderDetailss();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
