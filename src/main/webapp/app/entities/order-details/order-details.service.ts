import axios from 'axios';

import { IOrderDetails } from '@/shared/model/order-details.model';

const baseApiUrl = 'api/order-details';

export default class OrderDetailsService {
  public find(id: number): Promise<IOrderDetails> {
    return new Promise<IOrderDetails>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IOrderDetails): Promise<IOrderDetails> {
    return new Promise<IOrderDetails>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IOrderDetails): Promise<IOrderDetails> {
    return new Promise<IOrderDetails>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
