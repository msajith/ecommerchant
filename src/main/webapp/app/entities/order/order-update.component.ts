import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import { DATE_TIME_LONG_FORMAT, INSTANT_FORMAT, ZONED_DATE_TIME_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';
import { IOrder, Order } from '@/shared/model/order.model';
import OrderService from './order.service';

const validations: any = {
  order: {
    orderNumber: {},
    clientId: {},
    address: {},
    paymentGateway: {},
    conversionValue: {},
    createdAt: {},
    customerChoosenCurrency: {},
    customerCurrencySymbol: {},
    customerId: {},
    defaultPaymentCurrency: {},
    email: {},
    fullOrderStatus: {},
    isFinalize: {},
    items: {},
    orderDate: {},
    paymentId: {},
    paymentMethod: {},
    paymentStatus: {},
    totalPrice: {},
    updatedAt: {},
    earnedCoins: {},
    fakeItems: {},
    giftCardAmount: {},
    giftCardId: {},
    orderType: {},
    paymentDate: {},
    redeemCoinAmount: {},
    redeemCoins: {},
    totalAmountToPay: {},
    appliedCouponAmount: {},
    appliedPromoCode: {},
    shippingCharge: {},
    tax: {}
  }
};

@Component({
  validations
})
export default class OrderUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('orderService') private orderService: () => OrderService;
  public order: IOrder = new Order();
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderId) {
        vm.retrieveOrder(to.params.orderId);
      }
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.order.id) {
      this.orderService()
        .update(this.order)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.order.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.orderService()
        .create(this.order)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.order.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.order[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.order[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.order[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.order[field] = null;
    }
  }

  public retrieveOrder(orderId): void {
    this.orderService()
      .find(orderId)
      .then(res => {
        this.order = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
