import { Component, Inject, Vue } from 'vue-property-decorator';
import { IAppUser } from '@/shared/model/app-user.model';
import AlertService from '@/shared/alert/alert.service';

import AppUserService from './app-user.service';

@Component
export default class AppUser extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('appUserService') private appUserService: () => AppUserService;
  private removeId: string = null;
  public appUsers: IAppUser[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllAppUsers();
  }

  public clear(): void {
    this.retrieveAllAppUsers();
  }

  public retrieveAllAppUsers(): void {
    this.isFetching = true;

    this.appUserService()
      .retrieve()
      .then(
        res => {
          this.appUsers = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IAppUser): void {
    this.removeId = instance.customerId;
  }

  public removeAppUser(): void {
    this.appUserService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerchantApp.appUser.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllAppUsers();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
