import { Component, Vue, Inject } from 'vue-property-decorator';

import { IAppUser } from '@/shared/model/app-user.model';
import AppUserService from './app-user.service';

@Component
export default class AppUserDetails extends Vue {
  @Inject('appUserService') private appUserService: () => AppUserService;
  public appUser: IAppUser = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.appUserId) {
        vm.retrieveAppUser(to.params.appUserId);
      }
    });
  }

  public retrieveAppUser(appUserId) {
    this.appUserService()
      .find(appUserId)
      .then(res => {
        this.appUser = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
