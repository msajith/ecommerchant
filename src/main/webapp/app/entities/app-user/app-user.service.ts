import axios from 'axios';

import { IAppUser } from '@/shared/model/app-user.model';

const baseApiUrl = 'api/app-users';

export default class AppUserService {
  public find(id: string): Promise<IAppUser> {
    return new Promise<IAppUser>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IAppUser): Promise<IAppUser> {
    return new Promise<IAppUser>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IAppUser): Promise<IAppUser> {
    return new Promise<IAppUser>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
