import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import { DATE_TIME_LONG_FORMAT, INSTANT_FORMAT, ZONED_DATE_TIME_FORMAT } from '@/shared/date/filters';

import AppUserDeviceService from '../app-user-device/app-user-device.service';
import { IAppUserDevice } from '@/shared/model/app-user-device.model';

import AlertService from '@/shared/alert/alert.service';
import { IAppUser, AppUser } from '@/shared/model/app-user.model';
import AppUserService from './app-user.service';

const validations: any = {
  appUser: {
    customerId: {},
    clientId: {},
    identifierId: {},
    createdAt: {},
    email: {},
    firstName: {},
    isBlocked: {},
    isCustomer: {},
    isInactive: {},
    isVendor: {},
    lastLogin: {},
    lastName: {},
    loginOtp: {},
    loginStatus: {},
    loginCount: {},
    otpTimestamp: {},
    password: {},
    phone: {},
    resetToken: {},
    timestamp: {},
    updatedAt: {},
    userRole: {},
    userName: {},
    isAgent: {},
    isBankCustomer: {},
    verifiedAccount: {},
    lastLoginCountry: {},
    lastLoginDeviceId: {},
    lastLoginIpAddress: {},
    failureLoginAttempts: {}
  }
};

@Component({
  validations
})
export default class AppUserUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('appUserService') private appUserService: () => AppUserService;
  public appUser: IAppUser = new AppUser();

  @Inject('appUserDeviceService') private appUserDeviceService: () => AppUserDeviceService;

  public appUserDevices: IAppUserDevice[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.appUserId) {
        vm.retrieveAppUser(to.params.appUserId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.appUser.customerId) {
      this.appUserService()
        .update(this.appUser)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.appUser.updated', { param: param.customerId });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.appUserService()
        .create(this.appUser)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.appUser.created', { param: param.customerId });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date) {
      return format(date, DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.appUser[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.appUser[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.appUser[field] = parse(event.target.value, DATE_TIME_LONG_FORMAT, new Date());
    } else {
      this.appUser[field] = null;
    }
  }

  public retrieveAppUser(appUserId): void {
    this.appUserService()
      .find(appUserId)
      .then(res => {
        this.appUser = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.appUserDeviceService()
      .retrieve()
      .then(res => {
        this.appUserDevices = res.data;
      });
  }
}
