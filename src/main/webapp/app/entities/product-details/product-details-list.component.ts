import { Component, Inject, Vue } from 'vue-property-decorator';
import { IProductDetails } from '@/shared/model/product-details.model';
import AlertService from '@/shared/alert/alert.service';

import ProductDetailsService from './product-details.service';

@Component
export default class ProductDetailsList extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productDetailsService') private productDetailsService: () => ProductDetailsService;
  private removeId: number = null;
  public productDetails: IProductDetails[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllProductDetailss();
  }

  public clear(): void {
    this.retrieveAllProductDetailss();
  }

  public retrieveAllProductDetailss(): void {
    this.isFetching = true;

    this.productDetailsService()
      .retrieve()
      .then(
        res => {
          this.productDetails = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IProductDetails): void {
    this.removeId = instance.id;
  }

  public removeProductDetails(): void {
    this.productDetailsService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerchantApp.productDetails.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllProductDetailss();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
