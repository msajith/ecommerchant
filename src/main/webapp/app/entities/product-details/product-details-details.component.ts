import { Component, Vue, Inject } from 'vue-property-decorator';

import { IProductDetails } from '@/shared/model/product-details.model';
import ProductDetailsService from './product-details.service';

@Component
export default class ProductDetailsDetails extends Vue {
  @Inject('productDetailsService') private productDetailsService: () => ProductDetailsService;
  public productDetails: IProductDetails = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productDetailsId) {
        vm.retrieveProductDetails(to.params.productDetailsId);
      }
    });
  }

  public retrieveProductDetails(productDetailsId) {
    this.productDetailsService()
      .find(productDetailsId)
      .then(res => {
        this.productDetails = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
