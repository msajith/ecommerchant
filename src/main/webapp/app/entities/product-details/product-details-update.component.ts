import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import ProductService from '../product/product.service';
import { IProduct } from '@/shared/model/product.model';

import AlertService from '@/shared/alert/alert.service';
import { IProductDetails, ProductDetails } from '@/shared/model/product-details.model';
import ProductDetailsService from './product-details.service';

const validations: any = {
  productDetails: {
    clientId: {},
    adminKey: {},
    background: {},
    description: {},
    image1: {},
    image2: {},
    image3: {},
    image4: {},
    productCategoryId: {},
    recordStatus: {},
    shortDescription: {},
    unit: {},
    weight: {}
  }
};

@Component({
  validations
})
export default class ProductDetailsUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productDetailsService') private productDetailsService: () => ProductDetailsService;
  public productDetails: IProductDetails = new ProductDetails();

  @Inject('productService') private productService: () => ProductService;

  public products: IProduct[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productDetailsId) {
        vm.retrieveProductDetails(to.params.productDetailsId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.productDetails.id) {
      this.productDetailsService()
        .update(this.productDetails)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productDetails.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.productDetailsService()
        .create(this.productDetails)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productDetails.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveProductDetails(productDetailsId): void {
    this.productDetailsService()
      .find(productDetailsId)
      .then(res => {
        this.productDetails = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.productService()
      .retrieve()
      .then(res => {
        this.products = res.data;
      });
  }
}
