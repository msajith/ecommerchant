import axios from 'axios';

import { IProductDetails } from '@/shared/model/product-details.model';

const baseApiUrl = 'api/product-details';

export default class ProductDetailsService {
  public find(id: number): Promise<IProductDetails> {
    return new Promise<IProductDetails>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IProductDetails): Promise<IProductDetails> {
    return new Promise<IProductDetails>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IProductDetails): Promise<IProductDetails> {
    return new Promise<IProductDetails>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
