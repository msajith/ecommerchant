import { Component, Vue, Inject } from 'vue-property-decorator';

import { IProductDefinition } from '@/shared/model/product-definition.model';
import ProductDefinitionService from './product-definition.service';

@Component
export default class ProductDefinitionDetails extends Vue {
  @Inject('productDefinitionService') private productDefinitionService: () => ProductDefinitionService;
  public productDefinition: IProductDefinition = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productDefinitionId) {
        vm.retrieveProductDefinition(to.params.productDefinitionId);
      }
    });
  }

  public retrieveProductDefinition(productDefinitionId) {
    this.productDefinitionService()
      .find(productDefinitionId)
      .then(res => {
        this.productDefinition = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
