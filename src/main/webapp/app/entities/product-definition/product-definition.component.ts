import { Component, Inject, Vue } from 'vue-property-decorator';
import { IProductDefinition } from '@/shared/model/product-definition.model';
import AlertService from '@/shared/alert/alert.service';

import ProductDefinitionService from './product-definition.service';

@Component
export default class ProductDefinition extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productDefinitionService') private productDefinitionService: () => ProductDefinitionService;
  private removeId: number = null;
  public productDefinitions: IProductDefinition[] = [];

  public isFetching = false;
  public dismissCountDown: number = this.$store.getters.dismissCountDown;
  public dismissSecs: number = this.$store.getters.dismissSecs;
  public alertType: string = this.$store.getters.alertType;
  public alertMessage: any = this.$store.getters.alertMessage;

  public getAlertFromStore() {
    this.dismissCountDown = this.$store.getters.dismissCountDown;
    this.dismissSecs = this.$store.getters.dismissSecs;
    this.alertType = this.$store.getters.alertType;
    this.alertMessage = this.$store.getters.alertMessage;
  }

  public countDownChanged(dismissCountDown: number) {
    this.alertService().countDownChanged(dismissCountDown);
    this.getAlertFromStore();
  }

  public mounted(): void {
    this.retrieveAllProductDefinitions();
  }

  public clear(): void {
    this.retrieveAllProductDefinitions();
  }

  public retrieveAllProductDefinitions(): void {
    this.isFetching = true;

    this.productDefinitionService()
      .retrieve()
      .then(
        res => {
          this.productDefinitions = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IProductDefinition): void {
    this.removeId = instance.id;
  }

  public removeProductDefinition(): void {
    this.productDefinitionService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('ecommerchantApp.productDefinition.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();

        this.removeId = null;
        this.retrieveAllProductDefinitions();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
