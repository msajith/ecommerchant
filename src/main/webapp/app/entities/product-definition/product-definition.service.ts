import axios from 'axios';

import { IProductDefinition } from '@/shared/model/product-definition.model';

const baseApiUrl = 'api/product-definitions';

export default class ProductDefinitionService {
  public find(id: number): Promise<IProductDefinition> {
    return new Promise<IProductDefinition>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IProductDefinition): Promise<IProductDefinition> {
    return new Promise<IProductDefinition>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IProductDefinition): Promise<IProductDefinition> {
    return new Promise<IProductDefinition>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
