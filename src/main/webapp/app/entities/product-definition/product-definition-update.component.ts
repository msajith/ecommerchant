import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import ProductService from '../product/product.service';
import { IProduct } from '@/shared/model/product.model';

import OrderService from '../order/order.service';
import { IOrder } from '@/shared/model/order.model';

import AlertService from '@/shared/alert/alert.service';
import { IProductDefinition, ProductDefinition } from '@/shared/model/product-definition.model';
import ProductDefinitionService from './product-definition.service';

const validations: any = {
  productDefinition: {
    clientId: {},
    adminKey: {},
    productCategoryId: {},
    properties: {},
    propertyName: {},
    propertyValue: {},
    recordStatus: {},
    forFilter: {}
  }
};

@Component({
  validations
})
export default class ProductDefinitionUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productDefinitionService') private productDefinitionService: () => ProductDefinitionService;
  public productDefinition: IProductDefinition = new ProductDefinition();

  @Inject('productService') private productService: () => ProductService;

  public products: IProduct[] = [];

  @Inject('orderService') private orderService: () => OrderService;

  public orders: IOrder[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productDefinitionId) {
        vm.retrieveProductDefinition(to.params.productDefinitionId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.productDefinition.id) {
      this.productDefinitionService()
        .update(this.productDefinition)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productDefinition.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.productDefinitionService()
        .create(this.productDefinition)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productDefinition.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveProductDefinition(productDefinitionId): void {
    this.productDefinitionService()
      .find(productDefinitionId)
      .then(res => {
        this.productDefinition = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.productService()
      .retrieve()
      .then(res => {
        this.products = res.data;
      });
    this.orderService()
      .retrieve()
      .then(res => {
        this.orders = res.data;
      });
  }
}
