import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IProductCategory, ProductCategory } from '@/shared/model/product-category.model';
import ProductCategoryService from './product-category.service';

const validations: any = {
  productCategory: {
    clientId: {},
    adminKey: {},
    backgroundImage: {},
    categoryId: {},
    depthLevel: {},
    description: {},
    name: {},
    parentCategoryId: {},
    recordStatus: {}
  }
};

@Component({
  validations
})
export default class ProductCategoryUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productCategoryService') private productCategoryService: () => ProductCategoryService;
  public productCategory: IProductCategory = new ProductCategory();
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productCategoryId) {
        vm.retrieveProductCategory(to.params.productCategoryId);
      }
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.productCategory.id) {
      this.productCategoryService()
        .update(this.productCategory)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productCategory.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.productCategoryService()
        .create(this.productCategory)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productCategory.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveProductCategory(productCategoryId): void {
    this.productCategoryService()
      .find(productCategoryId)
      .then(res => {
        this.productCategory = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
