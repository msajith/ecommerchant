import axios from 'axios';

import { IProductCategory } from '@/shared/model/product-category.model';

const baseApiUrl = 'api/product-categories';

export default class ProductCategoryService {
  public find(id: number): Promise<IProductCategory> {
    return new Promise<IProductCategory>(resolve => {
      axios.get(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>(resolve => {
      axios.get(baseApiUrl).then(function(res) {
        resolve(res);
      });
    });
  }

  public delete(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      axios.delete(`${baseApiUrl}/${id}`).then(function(res) {
        resolve(res);
      });
    });
  }

  public create(entity: IProductCategory): Promise<IProductCategory> {
    return new Promise<IProductCategory>(resolve => {
      axios.post(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }

  public update(entity: IProductCategory): Promise<IProductCategory> {
    return new Promise<IProductCategory>(resolve => {
      axios.put(`${baseApiUrl}`, entity).then(function(res) {
        resolve(res.data);
      });
    });
  }
}
