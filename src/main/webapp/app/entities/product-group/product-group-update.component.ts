import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength } from 'vuelidate/lib/validators';

import ProductService from '../product/product.service';
import { IProduct } from '@/shared/model/product.model';

import AlertService from '@/shared/alert/alert.service';
import { IProductGroup, ProductGroup } from '@/shared/model/product-group.model';
import ProductGroupService from './product-group.service';

const validations: any = {
  productGroup: {
    clientId: {},
    adminKey: {},
    groupKey: {},
    groupValue: {},
    productCategoryId: {},
    productGroupId: {}
  }
};

@Component({
  validations
})
export default class ProductGroupUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('productGroupService') private productGroupService: () => ProductGroupService;
  public productGroup: IProductGroup = new ProductGroup();

  @Inject('productService') private productService: () => ProductService;

  public products: IProduct[] = [];
  public isSaving = false;

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productGroupId) {
        vm.retrieveProductGroup(to.params.productGroupId);
      }
      vm.initRelationships();
    });
  }

  public save(): void {
    this.isSaving = true;
    if (this.productGroup.id) {
      this.productGroupService()
        .update(this.productGroup)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productGroup.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.productGroupService()
        .create(this.productGroup)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('ecommerchantApp.productGroup.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveProductGroup(productGroupId): void {
    this.productGroupService()
      .find(productGroupId)
      .then(res => {
        this.productGroup = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.productService()
      .retrieve()
      .then(res => {
        this.products = res.data;
      });
  }
}
