export const enum FullOrderStatus {
  Delivered = 'Delivered',
  Shipped = 'Shipped',
  Processing = 'Processing'
}

export const enum OrderStatus {
  Delivered = 'Delivered',
  Shipped = 'Shipped',
  Processing = 'Processing'
}

export interface IOrderDetails {
  id?: number;
  orderDetailId?: number;
  clientId?: string;
  createdAt?: Date;
  customerAddress?: string;
  customerId?: string;
  deliveredDate?: Date;
  email?: string;
  fullOrderStatus?: FullOrderStatus;
  isProgressed?: boolean;
  isShipped?: boolean;
  orderDate?: Date;
  orderNumber?: string;
  orderStatus?: OrderStatus;
  paymentStatus?: string;
  price?: number;
  productId?: string;
  productImage?: string;
  productName?: string;
  quantity?: number;
  shippedDate?: Date;
  paymentMethod?: string;
  subTotal?: number;
  timeStamp?: Date;
  trackingId?: string;
  trackingUrl?: string;
  updatedAt?: Date;
  vendorId?: string;
  verificationKey?: string;
  earnedCoins?: number;
  giftCardAmount?: number;
  giftCardId?: string;
  orderType?: string;
  paymentDate?: Date;
  redeemCoinAmount?: number;
  redeemCoins?: number;
  shippingCharge?: number;
  tax?: number;
  orderTotal?: number;
  appliedCouponAmount?: number;
  appliedPromoCode?: string;
}

export class OrderDetails implements IOrderDetails {
  constructor(
    public id?: number,
    public orderDetailId?: number,
    public clientId?: string,
    public createdAt?: Date,
    public customerAddress?: string,
    public customerId?: string,
    public deliveredDate?: Date,
    public email?: string,
    public fullOrderStatus?: FullOrderStatus,
    public isProgressed?: boolean,
    public isShipped?: boolean,
    public orderDate?: Date,
    public orderNumber?: string,
    public orderStatus?: OrderStatus,
    public paymentStatus?: string,
    public price?: number,
    public productId?: string,
    public productImage?: string,
    public productName?: string,
    public quantity?: number,
    public shippedDate?: Date,
    public paymentMethod?: string,
    public subTotal?: number,
    public timeStamp?: Date,
    public trackingId?: string,
    public trackingUrl?: string,
    public updatedAt?: Date,
    public vendorId?: string,
    public verificationKey?: string,
    public earnedCoins?: number,
    public giftCardAmount?: number,
    public giftCardId?: string,
    public orderType?: string,
    public paymentDate?: Date,
    public redeemCoinAmount?: number,
    public redeemCoins?: number,
    public shippingCharge?: number,
    public tax?: number,
    public orderTotal?: number,
    public appliedCouponAmount?: number,
    public appliedPromoCode?: string
  ) {
    this.isProgressed = this.isProgressed || false;
    this.isShipped = this.isShipped || false;
  }
}
