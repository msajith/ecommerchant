import { IProduct } from '@/shared/model/product.model';
import { IOrder } from '@/shared/model/order.model';

export interface IProductDefinition {
  id?: number;
  clientId?: string;
  adminKey?: string;
  productCategoryId?: string;
  properties?: string;
  propertyName?: string;
  propertyValue?: string;
  recordStatus?: number;
  forFilter?: boolean;
  product?: IProduct;
  order?: IOrder;
}

export class ProductDefinition implements IProductDefinition {
  constructor(
    public id?: number,
    public clientId?: string,
    public adminKey?: string,
    public productCategoryId?: string,
    public properties?: string,
    public propertyName?: string,
    public propertyValue?: string,
    public recordStatus?: number,
    public forFilter?: boolean,
    public product?: IProduct,
    public order?: IOrder
  ) {
    this.forFilter = this.forFilter || false;
  }
}
