import { IProduct } from '@/shared/model/product.model';

export interface IProductInventory {
  id?: number;
  clientId?: string;
  availableStock?: number;
  costPrice?: number;
  damagedQuantity?: number;
  maxQuantity?: number;
  priceTag?: string;
  productCategoryId?: string;
  recordStatus?: number;
  reorderLevel?: number;
  unitPrice?: number;
  productId?: IProduct;
}

export class ProductInventory implements IProductInventory {
  constructor(
    public id?: number,
    public clientId?: string,
    public availableStock?: number,
    public costPrice?: number,
    public damagedQuantity?: number,
    public maxQuantity?: number,
    public priceTag?: string,
    public productCategoryId?: string,
    public recordStatus?: number,
    public reorderLevel?: number,
    public unitPrice?: number,
    public productId?: IProduct
  ) {}
}
