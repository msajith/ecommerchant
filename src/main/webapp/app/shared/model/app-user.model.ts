import { IAppUserDevice } from '@/shared/model/app-user-device.model';

export interface IAppUser {
  customerId?: string;
  clientId?: string;
  identifierId?: string;
  createdAt?: Date;
  email?: string;
  firstName?: string;
  isBlocked?: boolean;
  isCustomer?: boolean;
  isInactive?: boolean;
  isVendor?: boolean;
  lastLogin?: Date;
  lastName?: string;
  loginOtp?: string;
  loginStatus?: number;
  loginCount?: number;
  otpTimestamp?: Date;
  password?: string;
  phone?: string;
  resetToken?: string;
  timestamp?: Date;
  updatedAt?: Date;
  userRole?: string;
  userName?: string;
  isAgent?: boolean;
  isBankCustomer?: boolean;
  verifiedAccount?: boolean;
  lastLoginCountry?: string;
  lastLoginDeviceId?: string;
  lastLoginIpAddress?: string;
  failureLoginAttempts?: number;
  devices?: IAppUserDevice[];
}

export class AppUser implements IAppUser {
  constructor(
    public customerId?: string,
    public clientId?: string,
    public identifierId?: string,
    public createdAt?: Date,
    public email?: string,
    public firstName?: string,
    public isBlocked?: boolean,
    public isCustomer?: boolean,
    public isInactive?: boolean,
    public isVendor?: boolean,
    public lastLogin?: Date,
    public lastName?: string,
    public loginOtp?: string,
    public loginStatus?: number,
    public loginCount?: number,
    public otpTimestamp?: Date,
    public password?: string,
    public phone?: string,
    public resetToken?: string,
    public timestamp?: Date,
    public updatedAt?: Date,
    public userRole?: string,
    public userName?: string,
    public isAgent?: boolean,
    public isBankCustomer?: boolean,
    public verifiedAccount?: boolean,
    public lastLoginCountry?: string,
    public lastLoginDeviceId?: string,
    public lastLoginIpAddress?: string,
    public failureLoginAttempts?: number,
    public devices?: IAppUserDevice[]
  ) {
    this.isBlocked = this.isBlocked || false;
    this.isCustomer = this.isCustomer || false;
    this.isInactive = this.isInactive || false;
    this.isVendor = this.isVendor || false;
    this.isAgent = this.isAgent || false;
    this.isBankCustomer = this.isBankCustomer || false;
    this.verifiedAccount = this.verifiedAccount || false;
  }
}
