export interface IBrand {
  brandId?: string;
  name?: string;
}

export class Brand implements IBrand {
  constructor(public brandId?: string, public name?: string) {}
}
