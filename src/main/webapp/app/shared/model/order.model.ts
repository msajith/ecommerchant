export const enum FullOrderStatus {
  Delivered = 'Delivered',
  Shipped = 'Shipped',
  Processing = 'Processing'
}

export const enum PaymentStatus {
  SUCCESS = 'SUCCESS',
  PENDING = 'PENDING',
  FAILED = 'FAILED'
}

export interface IOrder {
  id?: number;
  orderNumber?: string;
  clientId?: string;
  address?: string;
  paymentGateway?: string;
  conversionValue?: number;
  createdAt?: Date;
  customerChoosenCurrency?: string;
  customerCurrencySymbol?: string;
  customerId?: string;
  defaultPaymentCurrency?: string;
  email?: string;
  fullOrderStatus?: FullOrderStatus;
  isFinalize?: boolean;
  items?: string;
  orderDate?: Date;
  paymentId?: string;
  paymentMethod?: string;
  paymentStatus?: PaymentStatus;
  totalPrice?: number;
  updatedAt?: Date;
  earnedCoins?: number;
  fakeItems?: string;
  giftCardAmount?: number;
  giftCardId?: string;
  orderType?: string;
  paymentDate?: Date;
  redeemCoinAmount?: number;
  redeemCoins?: number;
  totalAmountToPay?: number;
  appliedCouponAmount?: number;
  appliedPromoCode?: string;
  shippingCharge?: number;
  tax?: number;
}

export class Order implements IOrder {
  constructor(
    public id?: number,
    public orderNumber?: string,
    public clientId?: string,
    public address?: string,
    public paymentGateway?: string,
    public conversionValue?: number,
    public createdAt?: Date,
    public customerChoosenCurrency?: string,
    public customerCurrencySymbol?: string,
    public customerId?: string,
    public defaultPaymentCurrency?: string,
    public email?: string,
    public fullOrderStatus?: FullOrderStatus,
    public isFinalize?: boolean,
    public items?: string,
    public orderDate?: Date,
    public paymentId?: string,
    public paymentMethod?: string,
    public paymentStatus?: PaymentStatus,
    public totalPrice?: number,
    public updatedAt?: Date,
    public earnedCoins?: number,
    public fakeItems?: string,
    public giftCardAmount?: number,
    public giftCardId?: string,
    public orderType?: string,
    public paymentDate?: Date,
    public redeemCoinAmount?: number,
    public redeemCoins?: number,
    public totalAmountToPay?: number,
    public appliedCouponAmount?: number,
    public appliedPromoCode?: string,
    public shippingCharge?: number,
    public tax?: number
  ) {
    this.isFinalize = this.isFinalize || false;
  }
}
