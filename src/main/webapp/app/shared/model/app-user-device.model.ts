import { IAppUser } from '@/shared/model/app-user.model';

export const enum DeviceType {
  MOBILE = 'MOBILE',
  DESKTOP = 'DESKTOP',
  TABLET = 'TABLET',
  VOICE = 'VOICE'
}

export const enum DeviceStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
  SUSPENDED = 'SUSPENDED'
}

export interface IAppUserDevice {
  id?: number;
  deviceId?: string;
  deviceType?: DeviceType;
  deviceOs?: string;
  country?: string;
  ipAddressDuringRegistration?: string;
  createdAt?: Date;
  deviceKey?: string;
  status?: DeviceStatus;
  pin?: string;
  isLoggedIn?: boolean;
  lastLoginCountry?: string;
  lastLoginIpAddress?: string;
  appUser?: IAppUser;
}

export class AppUserDevice implements IAppUserDevice {
  constructor(
    public id?: number,
    public deviceId?: string,
    public deviceType?: DeviceType,
    public deviceOs?: string,
    public country?: string,
    public ipAddressDuringRegistration?: string,
    public createdAt?: Date,
    public deviceKey?: string,
    public status?: DeviceStatus,
    public pin?: string,
    public isLoggedIn?: boolean,
    public lastLoginCountry?: string,
    public lastLoginIpAddress?: string,
    public appUser?: IAppUser
  ) {
    this.isLoggedIn = this.isLoggedIn || false;
  }
}
