import { IProductCategory } from '@/shared/model/product-category.model';

export interface IProduct {
  id?: number;
  productId?: string;
  clientId?: string;
  adminKey?: string;
  createdAt?: Date;
  description?: string;
  modelCode?: string;
  name?: string;
  recordStatus?: number;
  salesQuantity?: number;
  tagCode?: string;
  updatedAt?: Date;
  indentifierId?: string;
  identifierId?: string;
  productGroupId?: string;
  productCategoryId?: IProductCategory;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public productId?: string,
    public clientId?: string,
    public adminKey?: string,
    public createdAt?: Date,
    public description?: string,
    public modelCode?: string,
    public name?: string,
    public recordStatus?: number,
    public salesQuantity?: number,
    public tagCode?: string,
    public updatedAt?: Date,
    public indentifierId?: string,
    public identifierId?: string,
    public productGroupId?: string,
    public productCategoryId?: IProductCategory
  ) {}
}
