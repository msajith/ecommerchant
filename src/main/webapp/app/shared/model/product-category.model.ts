export interface IProductCategory {
  id?: number;
  clientId?: string;
  adminKey?: string;
  backgroundImage?: string;
  categoryId?: string;
  depthLevel?: number;
  description?: string;
  name?: string;
  parentCategoryId?: string;
  recordStatus?: number;
}

export class ProductCategory implements IProductCategory {
  constructor(
    public id?: number,
    public clientId?: string,
    public adminKey?: string,
    public backgroundImage?: string,
    public categoryId?: string,
    public depthLevel?: number,
    public description?: string,
    public name?: string,
    public parentCategoryId?: string,
    public recordStatus?: number
  ) {}
}
