import { IProduct } from '@/shared/model/product.model';

export interface IProductDetails {
  id?: number;
  clientId?: string;
  adminKey?: string;
  background?: string;
  description?: string;
  image1?: string;
  image2?: string;
  image3?: string;
  image4?: string;
  productCategoryId?: string;
  recordStatus?: number;
  shortDescription?: string;
  unit?: string;
  weight?: number;
  productId?: IProduct;
}

export class ProductDetails implements IProductDetails {
  constructor(
    public id?: number,
    public clientId?: string,
    public adminKey?: string,
    public background?: string,
    public description?: string,
    public image1?: string,
    public image2?: string,
    public image3?: string,
    public image4?: string,
    public productCategoryId?: string,
    public recordStatus?: number,
    public shortDescription?: string,
    public unit?: string,
    public weight?: number,
    public productId?: IProduct
  ) {}
}
