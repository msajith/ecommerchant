import { IProduct } from '@/shared/model/product.model';

export interface IProductGroup {
  id?: number;
  clientId?: string;
  adminKey?: string;
  groupKey?: string;
  groupValue?: string;
  productCategoryId?: string;
  productGroupId?: string;
  productId?: IProduct;
}

export class ProductGroup implements IProductGroup {
  constructor(
    public id?: number,
    public clientId?: string,
    public adminKey?: string,
    public groupKey?: string,
    public groupValue?: string,
    public productCategoryId?: string,
    public productGroupId?: string,
    public productId?: IProduct
  ) {}
}
