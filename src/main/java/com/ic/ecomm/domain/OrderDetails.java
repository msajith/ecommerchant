package com.ic.ecomm.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ic.ecomm.domain.enumeration.FullOrderStatus;

import com.ic.ecomm.domain.enumeration.OrderStatus;

/**
 * A OrderDetails.
 */
@Entity
@Table(name = "order_details")
public class OrderDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "order_detail_id")
    private Integer orderDetailId;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "customer_address")
    private String customerAddress;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "delivered_date")
    private Instant deliveredDate;

    @Column(name = "email")
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "full_order_status")
    private FullOrderStatus fullOrderStatus;

    @Column(name = "is_progressed")
    private Boolean isProgressed;

    @Column(name = "is_shipped")
    private Boolean isShipped;

    @Column(name = "order_date")
    private Instant orderDate;

    @Column(name = "order_number")
    private String orderNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "order_status")
    private OrderStatus orderStatus;

    @Column(name = "payment_status")
    private String paymentStatus;

    @Column(name = "price", precision = 21, scale = 2)
    private BigDecimal price;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "product_image")
    private String productImage;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "shipped_date")
    private Instant shippedDate;

    @Column(name = "payment_method")
    private String paymentMethod;

    @Column(name = "sub_total", precision = 21, scale = 2)
    private BigDecimal subTotal;

    @Column(name = "time_stamp")
    private Instant timeStamp;

    @Column(name = "tracking_id")
    private String trackingId;

    @Column(name = "tracking_url")
    private String trackingUrl;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "vendor_id")
    private String vendorId;

    @Column(name = "verification_key")
    private String verificationKey;

    @Column(name = "earned_coins")
    private Integer earnedCoins;

    @Column(name = "gift_card_amount", precision = 21, scale = 2)
    private BigDecimal giftCardAmount;

    @Column(name = "gift_card_id")
    private String giftCardId;

    @Column(name = "order_type")
    private String orderType;

    @Column(name = "payment_date")
    private Instant paymentDate;

    @Column(name = "redeem_coin_amount", precision = 21, scale = 2)
    private BigDecimal redeemCoinAmount;

    @Column(name = "redeem_coins")
    private Integer redeemCoins;

    @Column(name = "shipping_charge", precision = 21, scale = 2)
    private BigDecimal shippingCharge;

    @Column(name = "tax", precision = 21, scale = 2)
    private BigDecimal tax;

    @Column(name = "order_total", precision = 21, scale = 2)
    private BigDecimal orderTotal;

    @Column(name = "applied_coupon_amount", precision = 21, scale = 2)
    private BigDecimal appliedCouponAmount;

    @Column(name = "applied_promo_code")
    private String appliedPromoCode;
    
    @ManyToOne
    @JsonIgnoreProperties("productDefinitions")
    private Order order;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getOrderDetailId() {
        return orderDetailId;
    }

    public OrderDetails orderDetailId(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
        return this;
    }

    public void setOrderDetailId(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getClientId() {
        return clientId;
    }

    public OrderDetails clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public OrderDetails createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public OrderDetails customerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
        return this;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerId() {
        return customerId;
    }

    public OrderDetails customerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Instant getDeliveredDate() {
        return deliveredDate;
    }

    public OrderDetails deliveredDate(Instant deliveredDate) {
        this.deliveredDate = deliveredDate;
        return this;
    }

    public void setDeliveredDate(Instant deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public String getEmail() {
        return email;
    }

    public OrderDetails email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public FullOrderStatus getFullOrderStatus() {
        return fullOrderStatus;
    }

    public OrderDetails fullOrderStatus(FullOrderStatus fullOrderStatus) {
        this.fullOrderStatus = fullOrderStatus;
        return this;
    }

    public void setFullOrderStatus(FullOrderStatus fullOrderStatus) {
        this.fullOrderStatus = fullOrderStatus;
    }

    public Boolean isIsProgressed() {
        return isProgressed;
    }

    public OrderDetails isProgressed(Boolean isProgressed) {
        this.isProgressed = isProgressed;
        return this;
    }

    public void setIsProgressed(Boolean isProgressed) {
        this.isProgressed = isProgressed;
    }

    public Boolean isIsShipped() {
        return isShipped;
    }

    public OrderDetails isShipped(Boolean isShipped) {
        this.isShipped = isShipped;
        return this;
    }

    public void setIsShipped(Boolean isShipped) {
        this.isShipped = isShipped;
    }

    public Instant getOrderDate() {
        return orderDate;
    }

    public OrderDetails orderDate(Instant orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public void setOrderDate(Instant orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public OrderDetails orderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public OrderDetails orderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public OrderDetails paymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
        return this;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public OrderDetails price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public OrderDetails productId(String productId) {
        this.productId = productId;
        return this;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public OrderDetails productImage(String productImage) {
        this.productImage = productImage;
        return this;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public OrderDetails productName(String productName) {
        this.productName = productName;
        return this;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public OrderDetails quantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Instant getShippedDate() {
        return shippedDate;
    }

    public OrderDetails shippedDate(Instant shippedDate) {
        this.shippedDate = shippedDate;
        return this;
    }

    public void setShippedDate(Instant shippedDate) {
        this.shippedDate = shippedDate;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public OrderDetails paymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public OrderDetails subTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
        return this;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public Instant getTimeStamp() {
        return timeStamp;
    }

    public OrderDetails timeStamp(Instant timeStamp) {
        this.timeStamp = timeStamp;
        return this;
    }

    public void setTimeStamp(Instant timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public OrderDetails trackingId(String trackingId) {
        this.trackingId = trackingId;
        return this;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getTrackingUrl() {
        return trackingUrl;
    }

    public OrderDetails trackingUrl(String trackingUrl) {
        this.trackingUrl = trackingUrl;
        return this;
    }

    public void setTrackingUrl(String trackingUrl) {
        this.trackingUrl = trackingUrl;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public OrderDetails updatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getVendorId() {
        return vendorId;
    }

    public OrderDetails vendorId(String vendorId) {
        this.vendorId = vendorId;
        return this;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVerificationKey() {
        return verificationKey;
    }

    public OrderDetails verificationKey(String verificationKey) {
        this.verificationKey = verificationKey;
        return this;
    }

    public void setVerificationKey(String verificationKey) {
        this.verificationKey = verificationKey;
    }

    public Integer getEarnedCoins() {
        return earnedCoins;
    }

    public OrderDetails earnedCoins(Integer earnedCoins) {
        this.earnedCoins = earnedCoins;
        return this;
    }

    public void setEarnedCoins(Integer earnedCoins) {
        this.earnedCoins = earnedCoins;
    }

    public BigDecimal getGiftCardAmount() {
        return giftCardAmount;
    }

    public OrderDetails giftCardAmount(BigDecimal giftCardAmount) {
        this.giftCardAmount = giftCardAmount;
        return this;
    }

    public void setGiftCardAmount(BigDecimal giftCardAmount) {
        this.giftCardAmount = giftCardAmount;
    }

    public String getGiftCardId() {
        return giftCardId;
    }

    public OrderDetails giftCardId(String giftCardId) {
        this.giftCardId = giftCardId;
        return this;
    }

    public void setGiftCardId(String giftCardId) {
        this.giftCardId = giftCardId;
    }

    public String getOrderType() {
        return orderType;
    }

    public OrderDetails orderType(String orderType) {
        this.orderType = orderType;
        return this;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Instant getPaymentDate() {
        return paymentDate;
    }

    public OrderDetails paymentDate(Instant paymentDate) {
        this.paymentDate = paymentDate;
        return this;
    }

    public void setPaymentDate(Instant paymentDate) {
        this.paymentDate = paymentDate;
    }

    public BigDecimal getRedeemCoinAmount() {
        return redeemCoinAmount;
    }

    public OrderDetails redeemCoinAmount(BigDecimal redeemCoinAmount) {
        this.redeemCoinAmount = redeemCoinAmount;
        return this;
    }

    public void setRedeemCoinAmount(BigDecimal redeemCoinAmount) {
        this.redeemCoinAmount = redeemCoinAmount;
    }

    public Integer getRedeemCoins() {
        return redeemCoins;
    }

    public OrderDetails redeemCoins(Integer redeemCoins) {
        this.redeemCoins = redeemCoins;
        return this;
    }

    public void setRedeemCoins(Integer redeemCoins) {
        this.redeemCoins = redeemCoins;
    }

    public BigDecimal getShippingCharge() {
        return shippingCharge;
    }

    public OrderDetails shippingCharge(BigDecimal shippingCharge) {
        this.shippingCharge = shippingCharge;
        return this;
    }

    public void setShippingCharge(BigDecimal shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public OrderDetails tax(BigDecimal tax) {
        this.tax = tax;
        return this;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public OrderDetails orderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
        return this;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    public BigDecimal getAppliedCouponAmount() {
        return appliedCouponAmount;
    }

    public OrderDetails appliedCouponAmount(BigDecimal appliedCouponAmount) {
        this.appliedCouponAmount = appliedCouponAmount;
        return this;
    }

    public void setAppliedCouponAmount(BigDecimal appliedCouponAmount) {
        this.appliedCouponAmount = appliedCouponAmount;
    }

    public String getAppliedPromoCode() {
        return appliedPromoCode;
    }

    public OrderDetails appliedPromoCode(String appliedPromoCode) {
        this.appliedPromoCode = appliedPromoCode;
        return this;
    }

    public void setAppliedPromoCode(String appliedPromoCode) {
        this.appliedPromoCode = appliedPromoCode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderDetails)) {
            return false;
        }
        return id != null && id.equals(((OrderDetails) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrderDetails{" +
            "id=" + getId() +
            ", orderDetailId=" + getOrderDetailId() +
            ", clientId='" + getClientId() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", customerAddress='" + getCustomerAddress() + "'" +
            ", customerId='" + getCustomerId() + "'" +
            ", deliveredDate='" + getDeliveredDate() + "'" +
            ", email='" + getEmail() + "'" +
            ", fullOrderStatus='" + getFullOrderStatus() + "'" +
            ", isProgressed='" + isIsProgressed() + "'" +
            ", isShipped='" + isIsShipped() + "'" +
            ", orderDate='" + getOrderDate() + "'" +
            ", orderNumber='" + getOrderNumber() + "'" +
            ", orderStatus='" + getOrderStatus() + "'" +
            ", paymentStatus='" + getPaymentStatus() + "'" +
            ", price=" + getPrice() +
            ", productId='" + getProductId() + "'" +
            ", productImage='" + getProductImage() + "'" +
            ", productName='" + getProductName() + "'" +
            ", quantity=" + getQuantity() +
            ", shippedDate='" + getShippedDate() + "'" +
            ", paymentMethod='" + getPaymentMethod() + "'" +
            ", subTotal=" + getSubTotal() +
            ", timeStamp='" + getTimeStamp() + "'" +
            ", trackingId='" + getTrackingId() + "'" +
            ", trackingUrl='" + getTrackingUrl() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", vendorId='" + getVendorId() + "'" +
            ", verificationKey='" + getVerificationKey() + "'" +
            ", earnedCoins=" + getEarnedCoins() +
            ", giftCardAmount=" + getGiftCardAmount() +
            ", giftCardId='" + getGiftCardId() + "'" +
            ", orderType='" + getOrderType() + "'" +
            ", paymentDate='" + getPaymentDate() + "'" +
            ", redeemCoinAmount=" + getRedeemCoinAmount() +
            ", redeemCoins=" + getRedeemCoins() +
            ", shippingCharge=" + getShippingCharge() +
            ", tax=" + getTax() +
            ", orderTotal=" + getOrderTotal() +
            ", appliedCouponAmount=" + getAppliedCouponAmount() +
            ", appliedPromoCode='" + getAppliedPromoCode() + "'" +
            "}";
    }
}
