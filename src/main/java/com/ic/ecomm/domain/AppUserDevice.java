package com.ic.ecomm.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

import com.ic.ecomm.domain.enumeration.DeviceType;

import com.ic.ecomm.domain.enumeration.DeviceStatus;

/**
 * A AppUserDevice.
 */
@Entity
@Table(name = "app_user_device")
public class AppUserDevice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "device_id")
    private String deviceId;

    @Enumerated(EnumType.STRING)
    @Column(name = "device_type")
    private DeviceType deviceType;

    @Column(name = "device_os")
    private String deviceOs;

    @Column(name = "country")
    private String country;

    @Column(name = "ip_address_during_registration")
    private String ipAddressDuringRegistration;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "device_key")
    private String deviceKey;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private DeviceStatus status;

    @Column(name = "pin")
    private String pin;

    @Column(name = "is_logged_in")
    private Boolean isLoggedIn;

    @Column(name = "last_login_country")
    private String lastLoginCountry;

    @Column(name = "last_login_ip_address")
    private String lastLoginIpAddress;

    @ManyToOne
    @JsonIgnoreProperties("devices")
    private AppUser appUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public AppUserDevice deviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public AppUserDevice deviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceOs() {
        return deviceOs;
    }

    public AppUserDevice deviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
        return this;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    public String getCountry() {
        return country;
    }

    public AppUserDevice country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIpAddressDuringRegistration() {
        return ipAddressDuringRegistration;
    }

    public AppUserDevice ipAddressDuringRegistration(String ipAddressDuringRegistration) {
        this.ipAddressDuringRegistration = ipAddressDuringRegistration;
        return this;
    }

    public void setIpAddressDuringRegistration(String ipAddressDuringRegistration) {
        this.ipAddressDuringRegistration = ipAddressDuringRegistration;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public AppUserDevice createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public AppUserDevice deviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
        return this;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public DeviceStatus getStatus() {
        return status;
    }

    public AppUserDevice status(DeviceStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(DeviceStatus status) {
        this.status = status;
    }

    public String getPin() {
        return pin;
    }

    public AppUserDevice pin(String pin) {
        this.pin = pin;
        return this;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Boolean isIsLoggedIn() {
        return isLoggedIn;
    }

    public AppUserDevice isLoggedIn(Boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
        return this;
    }

    public void setIsLoggedIn(Boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public String getLastLoginCountry() {
        return lastLoginCountry;
    }

    public AppUserDevice lastLoginCountry(String lastLoginCountry) {
        this.lastLoginCountry = lastLoginCountry;
        return this;
    }

    public void setLastLoginCountry(String lastLoginCountry) {
        this.lastLoginCountry = lastLoginCountry;
    }

    public String getLastLoginIpAddress() {
        return lastLoginIpAddress;
    }

    public AppUserDevice lastLoginIpAddress(String lastLoginIpAddress) {
        this.lastLoginIpAddress = lastLoginIpAddress;
        return this;
    }

    public void setLastLoginIpAddress(String lastLoginIpAddress) {
        this.lastLoginIpAddress = lastLoginIpAddress;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public AppUserDevice appUser(AppUser appUser) {
        this.appUser = appUser;
        return this;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AppUserDevice)) {
            return false;
        }
        return id != null && id.equals(((AppUserDevice) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AppUserDevice{" +
            "id=" + getId() +
            ", deviceId='" + getDeviceId() + "'" +
            ", deviceType='" + getDeviceType() + "'" +
            ", deviceOs='" + getDeviceOs() + "'" +
            ", country='" + getCountry() + "'" +
            ", ipAddressDuringRegistration='" + getIpAddressDuringRegistration() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", deviceKey='" + getDeviceKey() + "'" +
            ", status='" + getStatus() + "'" +
            ", pin='" + getPin() + "'" +
            ", isLoggedIn='" + isIsLoggedIn() + "'" +
            ", lastLoginCountry='" + getLastLoginCountry() + "'" +
            ", lastLoginIpAddress='" + getLastLoginIpAddress() + "'" +
            "}";
    }
}
