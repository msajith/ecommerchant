package com.ic.ecomm.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A ProductInventory.
 */
@Entity
@Table(name = "product_inventory")
public class ProductInventory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "client_id")
    private String clientId;

    @Column(name = "available_stock")
    private Integer availableStock;

    @Column(name = "cost_price", precision = 21, scale = 2)
    private BigDecimal costPrice;

    @Column(name = "damaged_quantity")
    private Integer damagedQuantity;

    @Column(name = "max_quantity")
    private Integer maxQuantity;

    @Column(name = "price_tag")
    private String priceTag;

    @Column(name = "product_category_id")
    private String productCategoryId;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "reorder_level")
    private Integer reorderLevel;

    @Column(name = "unit_price", precision = 21, scale = 2)
    private BigDecimal unitPrice;

    @Id
    @Column(name = "product_id")
    private String productId;

   
    public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getClientId() {
        return clientId;
    }

    public ProductInventory clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Integer getAvailableStock() {
        return availableStock;
    }

    public ProductInventory availableStock(Integer availableStock) {
        this.availableStock = availableStock;
        return this;
    }

    public void setAvailableStock(Integer availableStock) {
        this.availableStock = availableStock;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public ProductInventory costPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
        return this;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public Integer getDamagedQuantity() {
        return damagedQuantity;
    }

    public ProductInventory damagedQuantity(Integer damagedQuantity) {
        this.damagedQuantity = damagedQuantity;
        return this;
    }

    public void setDamagedQuantity(Integer damagedQuantity) {
        this.damagedQuantity = damagedQuantity;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public ProductInventory maxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
        return this;
    }

    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public String getPriceTag() {
        return priceTag;
    }

    public ProductInventory priceTag(String priceTag) {
        this.priceTag = priceTag;
        return this;
    }

    public void setPriceTag(String priceTag) {
        this.priceTag = priceTag;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public ProductInventory productCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
        return this;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Integer getRecordStatus() {
        return recordStatus;
    }

    public ProductInventory recordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Integer getReorderLevel() {
        return reorderLevel;
    }

    public ProductInventory reorderLevel(Integer reorderLevel) {
        this.reorderLevel = reorderLevel;
        return this;
    }

    public void setReorderLevel(Integer reorderLevel) {
        this.reorderLevel = reorderLevel;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public ProductInventory unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

  
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

   
}
