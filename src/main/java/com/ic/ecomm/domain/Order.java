package com.ic.ecomm.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

import com.ic.ecomm.domain.enumeration.FullOrderStatus;

import com.ic.ecomm.domain.enumeration.PaymentStatus;

/**
 * A Order.
 */
@Entity
@Table(name = "order_info")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "order_number")
    private String orderNumber;
    
    

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "address")
    private String address;

    @Column(name = "payment_gateway")
    private String paymentGateway;

    @Column(name = "conversion_value", precision = 21, scale = 2)
    private BigDecimal conversionValue;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "customer_choosen_currency")
    private String customerChoosenCurrency;

    @Column(name = "customer_currency_symbol")
    private String customerCurrencySymbol;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "default_payment_currency")
    private String defaultPaymentCurrency;

    @Column(name = "email")
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "full_order_status")
    private FullOrderStatus fullOrderStatus;

    @Column(name = "is_finalize")
    private Boolean isFinalize;

    @Column(name = "items")
    private String items;

    @Column(name = "order_date")
    private Instant orderDate;

    @Column(name = "payment_id")
    private String paymentId;

    @Column(name = "payment_method")
    private String paymentMethod;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @Column(name = "total_price")
    private Integer totalPrice;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "earned_coins")
    private Integer earnedCoins;

    @Column(name = "fake_items")
    private String fakeItems;

    @Column(name = "gift_card_amount")
    private Integer giftCardAmount;

    @Column(name = "gift_card_id")
    private String giftCardId;

    @Column(name = "order_type")
    private String orderType;

    @Column(name = "payment_date")
    private Instant paymentDate;

    @Column(name = "redeem_coin_amount")
    private Integer redeemCoinAmount;

    @Column(name = "redeem_coins")
    private Integer redeemCoins;

    @Column(name = "total_amount_to_pay")
    private Integer totalAmountToPay;

    @Column(name = "applied_coupon_amount")
    private Integer appliedCouponAmount;

    @Column(name = "applied_promo_code")
    private String appliedPromoCode;

    @Column(name = "shipping_charge", precision = 21, scale = 2)
    private BigDecimal shippingCharge;

    @Column(name = "tax", precision = 21, scale = 2)
    private BigDecimal tax;

  

    public String getOrderNumber() {
        return orderNumber;
    }

    public Order orderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getClientId() {
        return clientId;
    }

    public Order clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAddress() {
        return address;
    }

    public Order address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public Order paymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
        return this;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public BigDecimal getConversionValue() {
        return conversionValue;
    }

    public Order conversionValue(BigDecimal conversionValue) {
        this.conversionValue = conversionValue;
        return this;
    }

    public void setConversionValue(BigDecimal conversionValue) {
        this.conversionValue = conversionValue;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Order createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getCustomerChoosenCurrency() {
        return customerChoosenCurrency;
    }

    public Order customerChoosenCurrency(String customerChoosenCurrency) {
        this.customerChoosenCurrency = customerChoosenCurrency;
        return this;
    }

    public void setCustomerChoosenCurrency(String customerChoosenCurrency) {
        this.customerChoosenCurrency = customerChoosenCurrency;
    }

    public String getCustomerCurrencySymbol() {
        return customerCurrencySymbol;
    }

    public Order customerCurrencySymbol(String customerCurrencySymbol) {
        this.customerCurrencySymbol = customerCurrencySymbol;
        return this;
    }

    public void setCustomerCurrencySymbol(String customerCurrencySymbol) {
        this.customerCurrencySymbol = customerCurrencySymbol;
    }

    public String getCustomerId() {
        return customerId;
    }

    public Order customerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDefaultPaymentCurrency() {
        return defaultPaymentCurrency;
    }

    public Order defaultPaymentCurrency(String defaultPaymentCurrency) {
        this.defaultPaymentCurrency = defaultPaymentCurrency;
        return this;
    }

    public void setDefaultPaymentCurrency(String defaultPaymentCurrency) {
        this.defaultPaymentCurrency = defaultPaymentCurrency;
    }

    public String getEmail() {
        return email;
    }

    public Order email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public FullOrderStatus getFullOrderStatus() {
        return fullOrderStatus;
    }

    public Order fullOrderStatus(FullOrderStatus fullOrderStatus) {
        this.fullOrderStatus = fullOrderStatus;
        return this;
    }

    public void setFullOrderStatus(FullOrderStatus fullOrderStatus) {
        this.fullOrderStatus = fullOrderStatus;
    }

    public Boolean isIsFinalize() {
        return isFinalize;
    }

    public Order isFinalize(Boolean isFinalize) {
        this.isFinalize = isFinalize;
        return this;
    }

    public void setIsFinalize(Boolean isFinalize) {
        this.isFinalize = isFinalize;
    }

    public String getItems() {
        return items;
    }

    public Order items(String items) {
        this.items = items;
        return this;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public Instant getOrderDate() {
        return orderDate;
    }

    public Order orderDate(Instant orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public void setOrderDate(Instant orderDate) {
        this.orderDate = orderDate;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public Order paymentId(String paymentId) {
        this.paymentId = paymentId;
        return this;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public Order paymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public Order paymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
        return this;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public Order totalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public Order updatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getEarnedCoins() {
        return earnedCoins;
    }

    public Order earnedCoins(Integer earnedCoins) {
        this.earnedCoins = earnedCoins;
        return this;
    }

    public void setEarnedCoins(Integer earnedCoins) {
        this.earnedCoins = earnedCoins;
    }

    public String getFakeItems() {
        return fakeItems;
    }

    public Order fakeItems(String fakeItems) {
        this.fakeItems = fakeItems;
        return this;
    }

    public void setFakeItems(String fakeItems) {
        this.fakeItems = fakeItems;
    }

    public Integer getGiftCardAmount() {
        return giftCardAmount;
    }

    public Order giftCardAmount(Integer giftCardAmount) {
        this.giftCardAmount = giftCardAmount;
        return this;
    }

    public void setGiftCardAmount(Integer giftCardAmount) {
        this.giftCardAmount = giftCardAmount;
    }

    public String getGiftCardId() {
        return giftCardId;
    }

    public Order giftCardId(String giftCardId) {
        this.giftCardId = giftCardId;
        return this;
    }

    public void setGiftCardId(String giftCardId) {
        this.giftCardId = giftCardId;
    }

    public String getOrderType() {
        return orderType;
    }

    public Order orderType(String orderType) {
        this.orderType = orderType;
        return this;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Instant getPaymentDate() {
        return paymentDate;
    }

    public Order paymentDate(Instant paymentDate) {
        this.paymentDate = paymentDate;
        return this;
    }

    public void setPaymentDate(Instant paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Integer getRedeemCoinAmount() {
        return redeemCoinAmount;
    }

    public Order redeemCoinAmount(Integer redeemCoinAmount) {
        this.redeemCoinAmount = redeemCoinAmount;
        return this;
    }

    public void setRedeemCoinAmount(Integer redeemCoinAmount) {
        this.redeemCoinAmount = redeemCoinAmount;
    }

    public Integer getRedeemCoins() {
        return redeemCoins;
    }

    public Order redeemCoins(Integer redeemCoins) {
        this.redeemCoins = redeemCoins;
        return this;
    }

    public void setRedeemCoins(Integer redeemCoins) {
        this.redeemCoins = redeemCoins;
    }

    public Integer getTotalAmountToPay() {
        return totalAmountToPay;
    }

    public Order totalAmountToPay(Integer totalAmountToPay) {
        this.totalAmountToPay = totalAmountToPay;
        return this;
    }

    public void setTotalAmountToPay(Integer totalAmountToPay) {
        this.totalAmountToPay = totalAmountToPay;
    }

    public Integer getAppliedCouponAmount() {
        return appliedCouponAmount;
    }

    public Order appliedCouponAmount(Integer appliedCouponAmount) {
        this.appliedCouponAmount = appliedCouponAmount;
        return this;
    }

    public void setAppliedCouponAmount(Integer appliedCouponAmount) {
        this.appliedCouponAmount = appliedCouponAmount;
    }

    public String getAppliedPromoCode() {
        return appliedPromoCode;
    }

    public Order appliedPromoCode(String appliedPromoCode) {
        this.appliedPromoCode = appliedPromoCode;
        return this;
    }

    public void setAppliedPromoCode(String appliedPromoCode) {
        this.appliedPromoCode = appliedPromoCode;
    }

    public BigDecimal getShippingCharge() {
        return shippingCharge;
    }

    public Order shippingCharge(BigDecimal shippingCharge) {
        this.shippingCharge = shippingCharge;
        return this;
    }

    public void setShippingCharge(BigDecimal shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public Order tax(BigDecimal tax) {
        this.tax = tax;
        return this;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

   

  
}
