package com.ic.ecomm.domain.enumeration;

/**
 * The DeviceType enumeration.
 */
public enum DeviceType {
    MOBILE, DESKTOP, TABLET, VOICE
}
