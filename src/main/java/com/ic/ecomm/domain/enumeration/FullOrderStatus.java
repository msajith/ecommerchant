package com.ic.ecomm.domain.enumeration;

/**
 * The FullOrderStatus enumeration.
 */
public enum FullOrderStatus {
    Delivered, Shipped, Processing
}
