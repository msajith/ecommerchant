package com.ic.ecomm.domain.enumeration;

/**
 * The PaymentStatus enumeration.
 */
public enum PaymentStatus {
    SUCCESS, PENDING, FAILED
}
