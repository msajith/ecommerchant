package com.ic.ecomm.domain.enumeration;

/**
 * The DeviceStatus enumeration.
 */
public enum DeviceStatus {
    ACTIVE, INACTIVE, SUSPENDED
}
