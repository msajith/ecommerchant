package com.ic.ecomm.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    Delivered, Shipped, Processing
}
