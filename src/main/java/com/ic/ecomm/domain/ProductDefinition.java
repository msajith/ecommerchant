package com.ic.ecomm.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ProductDefinition.
 */
@Entity
@Table(name = "product_definition")
public class ProductDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "admin_key")
    private String adminKey;

    @Column(name = "product_category_id")
    private String productCategoryId;

    @Column(name = "properties")
    private String properties;

    @Column(name = "property_name")
    private String propertyName;

    @Column(name = "property_value")
    private String propertyValue;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "for_filter")
    private Boolean forFilter;

    @ManyToOne
    @JoinColumn(name = "product_id",referencedColumnName="product_id")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Product product;

  

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public ProductDefinition clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAdminKey() {
        return adminKey;
    }

    public ProductDefinition adminKey(String adminKey) {
        this.adminKey = adminKey;
        return this;
    }

    public void setAdminKey(String adminKey) {
        this.adminKey = adminKey;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public ProductDefinition productCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
        return this;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProperties() {
        return properties;
    }

    public ProductDefinition properties(String properties) {
        this.properties = properties;
        return this;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public ProductDefinition propertyName(String propertyName) {
        this.propertyName = propertyName;
        return this;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyValue() {
        return propertyValue;
    }

    public ProductDefinition propertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
        return this;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    public Integer getRecordStatus() {
        return recordStatus;
    }

    public ProductDefinition recordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean isForFilter() {
        return forFilter;
    }

    public ProductDefinition forFilter(Boolean forFilter) {
        this.forFilter = forFilter;
        return this;
    }

    public void setForFilter(Boolean forFilter) {
        this.forFilter = forFilter;
    }

    public Product getProduct() {
        return product;
    }

    public ProductDefinition product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

   
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductDefinition)) {
            return false;
        }
        return id != null && id.equals(((ProductDefinition) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProductDefinition{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", adminKey='" + getAdminKey() + "'" +
            ", productCategoryId='" + getProductCategoryId() + "'" +
            ", properties='" + getProperties() + "'" +
            ", propertyName='" + getPropertyName() + "'" +
            ", propertyValue='" + getPropertyValue() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", forFilter='" + isForFilter() + "'" +
            "}";
    }
}
