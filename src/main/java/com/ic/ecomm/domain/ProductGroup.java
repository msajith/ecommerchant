package com.ic.ecomm.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ProductGroup.
 */
@Entity
@Table(name = "product_group")
public class ProductGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "admin_key")
    private String adminKey;

    @Column(name = "group_key")
    private String groupKey;

    @Column(name = "group_value")
    private String groupValue;

    @Column(name = "product_category_id")
    private String productCategoryId;

    @Column(name = "product_group_id")
    private String productGroupId;

    @ManyToOne
    @JsonIgnoreProperties("productGroups")
    @JoinColumn(name = "product_id",referencedColumnName="product_id")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Product product;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public ProductGroup clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAdminKey() {
        return adminKey;
    }

    public ProductGroup adminKey(String adminKey) {
        this.adminKey = adminKey;
        return this;
    }

    public void setAdminKey(String adminKey) {
        this.adminKey = adminKey;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public ProductGroup groupKey(String groupKey) {
        this.groupKey = groupKey;
        return this;
    }

    public void setGroupKey(String groupKey) {
        this.groupKey = groupKey;
    }

    public String getGroupValue() {
        return groupValue;
    }

    public ProductGroup groupValue(String groupValue) {
        this.groupValue = groupValue;
        return this;
    }

    public void setGroupValue(String groupValue) {
        this.groupValue = groupValue;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public ProductGroup productCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
        return this;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductGroupId() {
        return productGroupId;
    }

    public ProductGroup productGroupId(String productGroupId) {
        this.productGroupId = productGroupId;
        return this;
    }

    public void setProductGroupId(String productGroupId) {
        this.productGroupId = productGroupId;
    }

    public Product getProduct() {
        return product;
    }

    public ProductGroup product(Product product) {
        this.product = product;
        return this;
    }

    public void setProductId(Product product) {
        this.product = product;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductGroup)) {
            return false;
        }
        return id != null && id.equals(((ProductGroup) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProductGroup{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", adminKey='" + getAdminKey() + "'" +
            ", groupKey='" + getGroupKey() + "'" +
            ", groupValue='" + getGroupValue() + "'" +
            ", productCategoryId='" + getProductCategoryId() + "'" +
            ", productGroupId='" + getProductGroupId() + "'" +
            "}";
    }
}
