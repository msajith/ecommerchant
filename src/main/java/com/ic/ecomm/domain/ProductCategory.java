package com.ic.ecomm.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A ProductCategory.
 */
@Entity
@Table(name = "product_category")
public class ProductCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "admin_key")
    private String adminKey;

    @Column(name = "background_image")
    private String backgroundImage;

    @Column(name = "category_id")
    private String categoryId;

    @Column(name = "dept_level")
    private Integer depthLevel;

    @Column(name = "description")
    private String description;

    @Column(name = "name")
    private String name;

    @Column(name = "parent_category_id")
    private String parentCategoryId;

    @Column(name = "record_status")
    private Integer recordStatus;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public ProductCategory clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAdminKey() {
        return adminKey;
    }

    public ProductCategory adminKey(String adminKey) {
        this.adminKey = adminKey;
        return this;
    }

    public void setAdminKey(String adminKey) {
        this.adminKey = adminKey;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public ProductCategory backgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
        return this;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public ProductCategory categoryId(String categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getDepthLevel() {
        return depthLevel;
    }

    public ProductCategory depthLevel(Integer depthLevel) {
        this.depthLevel = depthLevel;
        return this;
    }

    public void setDepthLevel(Integer depthLevel) {
        this.depthLevel = depthLevel;
    }

    public String getDescription() {
        return description;
    }

    public ProductCategory description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public ProductCategory name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentCategoryId() {
        return parentCategoryId;
    }

    public ProductCategory parentCategoryId(String parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
        return this;
    }

    public void setParentCategoryId(String parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public Integer getRecordStatus() {
        return recordStatus;
    }

    public ProductCategory recordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductCategory)) {
            return false;
        }
        return id != null && id.equals(((ProductCategory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProductCategory{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", adminKey='" + getAdminKey() + "'" +
            ", backgroundImage='" + getBackgroundImage() + "'" +
            ", categoryId='" + getCategoryId() + "'" +
            ", depthLevel=" + getDepthLevel() +
            ", description='" + getDescription() + "'" +
            ", name='" + getName() + "'" +
            ", parentCategoryId='" + getParentCategoryId() + "'" +
            ", recordStatus=" + getRecordStatus() +
            "}";
    }
}
