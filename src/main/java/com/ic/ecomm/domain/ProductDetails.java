package com.ic.ecomm.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A ProductDetails.
 */
@Entity
@Table(name = "product_details")
public class ProductDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "admin_key")
    private String adminKey;

    @Column(name = "background")
    private String background;

    @Column(name = "description")
    private String description;

    @Column(name = "image1")
    private String image1;

    @Column(name = "image2")
    private String image2;

    @Column(name = "image3")
    private String image3;

    @Column(name = "image4")
    private String image4;

    @Column(name = "product_category_id")
    private String productCategoryId;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "unit")
    private String unit;

    @Column(name = "weight", precision = 21, scale = 2)
    private BigDecimal weight;

    @OneToOne
    @JsonIgnoreProperties("productDetails")
    @JoinColumn(name = "product_id",referencedColumnName="product_id")
    @org.hibernate.annotations.ForeignKey(name = "none")
    private Product productId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public ProductDetails clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getAdminKey() {
        return adminKey;
    }

    public ProductDetails adminKey(String adminKey) {
        this.adminKey = adminKey;
        return this;
    }

    public void setAdminKey(String adminKey) {
        this.adminKey = adminKey;
    }

    public String getBackground() {
        return background;
    }

    public ProductDetails background(String background) {
        this.background = background;
        return this;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getDescription() {
        return description;
    }

    public ProductDetails description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage1() {
        return image1;
    }

    public ProductDetails image1(String image1) {
        this.image1 = image1;
        return this;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public ProductDetails image2(String image2) {
        this.image2 = image2;
        return this;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public ProductDetails image3(String image3) {
        this.image3 = image3;
        return this;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public ProductDetails image4(String image4) {
        this.image4 = image4;
        return this;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public ProductDetails productCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
        return this;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Integer getRecordStatus() {
        return recordStatus;
    }

    public ProductDetails recordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public ProductDetails shortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getUnit() {
        return unit;
    }

    public ProductDetails unit(String unit) {
        this.unit = unit;
        return this;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public ProductDetails weight(BigDecimal weight) {
        this.weight = weight;
        return this;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Product getProductId() {
        return productId;
    }

    public ProductDetails productId(Product product) {
        this.productId = product;
        return this;
    }

    public void setProductId(Product product) {
        this.productId = product;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductDetails)) {
            return false;
        }
        return id != null && id.equals(((ProductDetails) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProductDetails{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", adminKey='" + getAdminKey() + "'" +
            ", background='" + getBackground() + "'" +
            ", description='" + getDescription() + "'" +
            ", image1='" + getImage1() + "'" +
            ", image2='" + getImage2() + "'" +
            ", image3='" + getImage3() + "'" +
            ", image4='" + getImage4() + "'" +
            ", productCategoryId='" + getProductCategoryId() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", shortDescription='" + getShortDescription() + "'" +
            ", unit='" + getUnit() + "'" +
            ", weight=" + getWeight() +
            "}";
    }
}
