package com.ic.ecomm.repository;

import com.ic.ecomm.domain.ProductGroup;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProductGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductGroupRepository extends JpaRepository<ProductGroup, Long> {

}
