package com.ic.ecomm.repository;

import com.ic.ecomm.domain.ProductDefinition;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProductDefinition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductDefinitionRepository extends JpaRepository<ProductDefinition, Long> {

}
