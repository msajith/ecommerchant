package com.ic.ecomm.repository;

import com.ic.ecomm.domain.AppUserDevice;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AppUserDevice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AppUserDeviceRepository extends JpaRepository<AppUserDevice, Long> {

}
