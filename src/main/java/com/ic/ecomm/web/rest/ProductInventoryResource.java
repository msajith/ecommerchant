package com.ic.ecomm.web.rest;

import com.ic.ecomm.domain.ProductInventory;
import com.ic.ecomm.repository.ProductInventoryRepository;
import com.ic.ecomm.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ic.ecomm.domain.ProductInventory}.
 */
@RestController
@RequestMapping("/api")
public class ProductInventoryResource {

    private final Logger log = LoggerFactory.getLogger(ProductInventoryResource.class);

    private static final String ENTITY_NAME = "productInventory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductInventoryRepository productInventoryRepository;

    public ProductInventoryResource(ProductInventoryRepository productInventoryRepository) {
        this.productInventoryRepository = productInventoryRepository;
    }

    /**
     * {@code POST  /product-inventories} : Create a new productInventory.
     *
     * @param productInventory the productInventory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productInventory, or with status {@code 400 (Bad Request)} if the productInventory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-inventories")
    public ResponseEntity<ProductInventory> createProductInventory(@RequestBody ProductInventory productInventory) throws URISyntaxException {
        log.debug("REST request to save ProductInventory : {}", productInventory);
        if (productInventory.getProductId() != null) {
            throw new BadRequestAlertException("A new productInventory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductInventory result = productInventoryRepository.save(productInventory);
        return ResponseEntity.created(new URI("/api/product-inventories/" + result.getProductId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getProductId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-inventories} : Updates an existing productInventory.
     *
     * @param productInventory the productInventory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productInventory,
     * or with status {@code 400 (Bad Request)} if the productInventory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productInventory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-inventories")
    public ResponseEntity<ProductInventory> updateProductInventory(@RequestBody ProductInventory productInventory) throws URISyntaxException {
        log.debug("REST request to update ProductInventory : {}", productInventory);
        if (productInventory.getProductId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductInventory result = productInventoryRepository.save(productInventory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productInventory.getProductId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-inventories} : get all the productInventories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productInventories in body.
     */
    @GetMapping("/product-inventories")
    public List<ProductInventory> getAllProductInventories() {
        log.debug("REST request to get all ProductInventories");
        return productInventoryRepository.findAll();
    }

    /**
     * {@code GET  /product-inventories/:id} : get the "id" productInventory.
     *
     * @param id the id of the productInventory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productInventory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-inventories/{id}")
    public ResponseEntity<ProductInventory> getProductInventory(@PathVariable Long id) {
        log.debug("REST request to get ProductInventory : {}", id);
        Optional<ProductInventory> productInventory = productInventoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productInventory);
    }

    /**
     * {@code DELETE  /product-inventories/:id} : delete the "id" productInventory.
     *
     * @param id the id of the productInventory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-inventories/{id}")
    public ResponseEntity<Void> deleteProductInventory(@PathVariable Long id) {
        log.debug("REST request to delete ProductInventory : {}", id);
        productInventoryRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
