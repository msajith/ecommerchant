package com.ic.ecomm.web.rest;

import com.ic.ecomm.domain.ProductDefinition;
import com.ic.ecomm.repository.ProductDefinitionRepository;
import com.ic.ecomm.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ic.ecomm.domain.ProductDefinition}.
 */
@RestController
@RequestMapping("/api")
public class ProductDefinitionResource {

    private final Logger log = LoggerFactory.getLogger(ProductDefinitionResource.class);

    private static final String ENTITY_NAME = "productDefinition";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductDefinitionRepository productDefinitionRepository;

    public ProductDefinitionResource(ProductDefinitionRepository productDefinitionRepository) {
        this.productDefinitionRepository = productDefinitionRepository;
    }

    /**
     * {@code POST  /product-definitions} : Create a new productDefinition.
     *
     * @param productDefinition the productDefinition to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productDefinition, or with status {@code 400 (Bad Request)} if the productDefinition has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-definitions")
    public ResponseEntity<ProductDefinition> createProductDefinition(@RequestBody ProductDefinition productDefinition) throws URISyntaxException {
        log.debug("REST request to save ProductDefinition : {}", productDefinition);
        if (productDefinition.getId() != null) {
            throw new BadRequestAlertException("A new productDefinition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductDefinition result = productDefinitionRepository.save(productDefinition);
        return ResponseEntity.created(new URI("/api/product-definitions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-definitions} : Updates an existing productDefinition.
     *
     * @param productDefinition the productDefinition to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productDefinition,
     * or with status {@code 400 (Bad Request)} if the productDefinition is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productDefinition couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-definitions")
    public ResponseEntity<ProductDefinition> updateProductDefinition(@RequestBody ProductDefinition productDefinition) throws URISyntaxException {
        log.debug("REST request to update ProductDefinition : {}", productDefinition);
        if (productDefinition.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductDefinition result = productDefinitionRepository.save(productDefinition);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productDefinition.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-definitions} : get all the productDefinitions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productDefinitions in body.
     */
    @GetMapping("/product-definitions")
    public List<ProductDefinition> getAllProductDefinitions() {
        log.debug("REST request to get all ProductDefinitions");
        return productDefinitionRepository.findAll();
    }

    /**
     * {@code GET  /product-definitions/:id} : get the "id" productDefinition.
     *
     * @param id the id of the productDefinition to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productDefinition, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-definitions/{id}")
    public ResponseEntity<ProductDefinition> getProductDefinition(@PathVariable Long id) {
        log.debug("REST request to get ProductDefinition : {}", id);
        Optional<ProductDefinition> productDefinition = productDefinitionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productDefinition);
    }

    /**
     * {@code DELETE  /product-definitions/:id} : delete the "id" productDefinition.
     *
     * @param id the id of the productDefinition to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-definitions/{id}")
    public ResponseEntity<Void> deleteProductDefinition(@PathVariable Long id) {
        log.debug("REST request to delete ProductDefinition : {}", id);
        productDefinitionRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
