package com.ic.ecomm.web.rest;

import com.ic.ecomm.domain.AppUserDevice;
import com.ic.ecomm.repository.AppUserDeviceRepository;
import com.ic.ecomm.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ic.ecomm.domain.AppUserDevice}.
 */
@RestController
@RequestMapping("/api")
public class AppUserDeviceResource {

    private final Logger log = LoggerFactory.getLogger(AppUserDeviceResource.class);

    private static final String ENTITY_NAME = "appUserDevice";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AppUserDeviceRepository appUserDeviceRepository;

    public AppUserDeviceResource(AppUserDeviceRepository appUserDeviceRepository) {
        this.appUserDeviceRepository = appUserDeviceRepository;
    }

    /**
     * {@code POST  /app-user-devices} : Create a new appUserDevice.
     *
     * @param appUserDevice the appUserDevice to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new appUserDevice, or with status {@code 400 (Bad Request)} if the appUserDevice has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/app-user-devices")
    public ResponseEntity<AppUserDevice> createAppUserDevice(@RequestBody AppUserDevice appUserDevice) throws URISyntaxException {
        log.debug("REST request to save AppUserDevice : {}", appUserDevice);
        if (appUserDevice.getId() != null) {
            throw new BadRequestAlertException("A new appUserDevice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AppUserDevice result = appUserDeviceRepository.save(appUserDevice);
        return ResponseEntity.created(new URI("/api/app-user-devices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /app-user-devices} : Updates an existing appUserDevice.
     *
     * @param appUserDevice the appUserDevice to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated appUserDevice,
     * or with status {@code 400 (Bad Request)} if the appUserDevice is not valid,
     * or with status {@code 500 (Internal Server Error)} if the appUserDevice couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/app-user-devices")
    public ResponseEntity<AppUserDevice> updateAppUserDevice(@RequestBody AppUserDevice appUserDevice) throws URISyntaxException {
        log.debug("REST request to update AppUserDevice : {}", appUserDevice);
        if (appUserDevice.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AppUserDevice result = appUserDeviceRepository.save(appUserDevice);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, appUserDevice.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /app-user-devices} : get all the appUserDevices.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of appUserDevices in body.
     */
    @GetMapping("/app-user-devices")
    public List<AppUserDevice> getAllAppUserDevices() {
        log.debug("REST request to get all AppUserDevices");
        return appUserDeviceRepository.findAll();
    }

    /**
     * {@code GET  /app-user-devices/:id} : get the "id" appUserDevice.
     *
     * @param id the id of the appUserDevice to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the appUserDevice, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/app-user-devices/{id}")
    public ResponseEntity<AppUserDevice> getAppUserDevice(@PathVariable Long id) {
        log.debug("REST request to get AppUserDevice : {}", id);
        Optional<AppUserDevice> appUserDevice = appUserDeviceRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(appUserDevice);
    }

    /**
     * {@code DELETE  /app-user-devices/:id} : delete the "id" appUserDevice.
     *
     * @param id the id of the appUserDevice to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/app-user-devices/{id}")
    public ResponseEntity<Void> deleteAppUserDevice(@PathVariable Long id) {
        log.debug("REST request to delete AppUserDevice : {}", id);
        appUserDeviceRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
