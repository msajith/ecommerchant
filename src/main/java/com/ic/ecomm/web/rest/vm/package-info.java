/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ic.ecomm.web.rest.vm;
